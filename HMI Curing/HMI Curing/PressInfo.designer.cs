﻿namespace HMI_Curing
{
    partial class PressInfo
    {
        /// <summary> 
        /// RequiIndianRed designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// RequiIndianRed method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Lbl_Press = new System.Windows.Forms.Label();
            this.Lbl_BD = new System.Windows.Forms.Label();
            this.Lbl_NGT = new System.Windows.Forms.Label();
            this.Lbl_QA = new System.Windows.Forms.Label();
            this.Lbl_Setups = new System.Windows.Forms.Label();
            this.Lbl_Molds = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Lbl_Press
            // 
            this.Lbl_Press.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Lbl_Press.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_Press.Location = new System.Drawing.Point(0, 0);
            this.Lbl_Press.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Lbl_Press.Name = "Lbl_Press";
            this.Lbl_Press.Size = new System.Drawing.Size(165, 30);
            this.Lbl_Press.TabIndex = 0;
            this.Lbl_Press.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lbl_BD
            // 
            this.Lbl_BD.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Lbl_BD.Location = new System.Drawing.Point(225, 0);
            this.Lbl_BD.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Lbl_BD.Name = "Lbl_BD";
            this.Lbl_BD.Size = new System.Drawing.Size(20, 30);
            this.Lbl_BD.TabIndex = 1;
            // 
            // Lbl_NGT
            // 
            this.Lbl_NGT.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Lbl_NGT.Location = new System.Drawing.Point(245, 0);
            this.Lbl_NGT.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Lbl_NGT.Name = "Lbl_NGT";
            this.Lbl_NGT.Size = new System.Drawing.Size(20, 30);
            this.Lbl_NGT.TabIndex = 2;
            // 
            // Lbl_QA
            // 
            this.Lbl_QA.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Lbl_QA.Location = new System.Drawing.Point(205, 0);
            this.Lbl_QA.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Lbl_QA.Name = "Lbl_QA";
            this.Lbl_QA.Size = new System.Drawing.Size(20, 30);
            this.Lbl_QA.TabIndex = 3;
            // 
            // Lbl_Setups
            // 
            this.Lbl_Setups.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Lbl_Setups.Location = new System.Drawing.Point(185, 0);
            this.Lbl_Setups.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Lbl_Setups.Name = "Lbl_Setups";
            this.Lbl_Setups.Size = new System.Drawing.Size(20, 30);
            this.Lbl_Setups.TabIndex = 4;
            // 
            // Lbl_Molds
            // 
            this.Lbl_Molds.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Lbl_Molds.Location = new System.Drawing.Point(165, 0);
            this.Lbl_Molds.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Lbl_Molds.Name = "Lbl_Molds";
            this.Lbl_Molds.Size = new System.Drawing.Size(20, 30);
            this.Lbl_Molds.TabIndex = 5;
            // 
            // PressInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Lbl_Molds);
            this.Controls.Add(this.Lbl_Setups);
            this.Controls.Add(this.Lbl_QA);
            this.Controls.Add(this.Lbl_NGT);
            this.Controls.Add(this.Lbl_BD);
            this.Controls.Add(this.Lbl_Press);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "PressInfo";
            this.Size = new System.Drawing.Size(267, 30);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label Lbl_Press;
        private System.Windows.Forms.Label Lbl_BD;
        private System.Windows.Forms.Label Lbl_NGT;
        private System.Windows.Forms.Label Lbl_QA;
        private System.Windows.Forms.Label Lbl_Setups;
        private System.Windows.Forms.Label Lbl_Molds;
    }
}
