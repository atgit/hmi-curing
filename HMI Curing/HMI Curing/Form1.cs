﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;
using System.Diagnostics;
using System.Globalization;
using cwbx;

using ClassLibrary;
using System.Timers;

namespace HMI_Curing
{
    public partial class Form1 : Form
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        int i = 0;
        int j = 0;
        int x = 0;
        int interval = 0;
        string StrSql = "";
        public static MachineInfo[] Arr_Lbl;
        public static List<Press> machineList = new List<Press>();

        DataTable DT = new DataTable();
        string Lib = "rzpali";
        public static string Lib1 = "rzpali";
        public static string tapiali = "tapiali";
        public bool IsServer;
        //string Lib = "rzpali";
        int Counter = 0;
        public static Form1 inst;
        public double TimerCount = 0;
        public static bool loggedIn; // האם המנהל המדווח מחובר למערכת
        public static string WorkerName = "";
        public static string name = ""; // מנהל המחובר למערכת

        public static List<Press> PressRegisters = new List<Press>(); // רשימת כתובות הרגיסטרים בבקרים
        public static PLC Pl;

        System.Timers.Timer aTimer = new System.Timers.Timer();

        /// <summary>
        ///  Test
        /// </summary>
        public Form1()
        {
            inst = this;
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Press P = new Press("507");
            //P.Department = "161";
            //ChooseMold choose = new ChooseMold(P);
            //choose.ShowDialog();
            DBService dbs = new DBService();
            if (System.IO.File.Exists(@"C:\c#\HMI Curing.txt"))
            {
                IsServer = true;
                ///// התממשקות עם בקרים
                Pl = new PLC();
                //Pl.Connect();
                GetPLCAddresses();
            }

            Fill_MachineList(Initialize());
            //// יצירת לייבלים של מכונות

            Arr_Lbl = new MachineInfo[machineList.Count];
            j = 0;
            x = 0;
            CreateLBL();
            ResizeMachiInfo();

            /////
            if (IsServer)
                UpdateTables();

            if (!IsServer)
            {
                // סימון מכבשים שלא קראו טעינה.
                foreach (Press item in machineList)
                {
                    if (item.ActionCode == 1 && !item.DidReadDNA())
                    {
                        var index = machineList.FindIndex(row1 => row1.MacName.ToString().Trim() == item.MacName.ToString().Trim());
                        Arr_Lbl[index].DNA_Unread_Color();
                    }
                }
            }
           
            //// נפעיל את הטיימר פעם ראשונה כדי לעדכן את הטבלאות בעליית התוכנית
            timer1_Tick(null, null);
            ColorAllLBL();
            IsOpeningSoon();
            // נריץ בדיקת על המכבשים עם תקלות חוסר תכנון וחוסר צמיגים כדי להבהב אם כבר נבנו להם צמיגים מתאימים
            CheckForTires();
            timer1.Enabled = true;
            timer2.Enabled = true;
            //EndOfShiftTimer.Enabled = IsServer ? true : false;
            Lbl_Update.Text = $"Last Update: {DateTime.Now.ToLongTimeString()}";

            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.Interval = 60000;
            aTimer.Enabled = IsServer ? true : false;
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            // טיימר לעדכון וסגירת תקלות בסוף כל משמרת. התקלות הפתוחות ייסגרו,
            //יעודכן לכל תקלה כמה זמן היא ארכה עד כה, והתקלות הפתוחות ייפתחו מחדש למשמרת העוקבת
            string morning = new TimeSpan(6, 30, 0).ToString("hhmm");
            string noon = new TimeSpan(15, 00, 0).ToString("hhmm");
            string night = new TimeSpan(23, 30, 0).ToString("hhmm");
            string now = DateTime.Now.TimeOfDay.ToString("hhmm");
            DateTime current = DateTime.Now;

            if (now == morning || now == noon || now == night)
            {
                log.Info("Entered end of shift");
                List<Press> list = machineList.Where(x => x.ActionCode == 4).ToList();
                foreach (Press row in list)
                {
                    log.Info($@"Press In Action code = 4 - {row.MacName}");
                    inst.EndOfTak(Array.FindIndex(Arr_Lbl, row1 => row1.Mach.Trim() == row.MacName.Trim()));
                    // סגירת התקלה ופתיחתה מחדש לצורך הפרדה למשמרות
                    row.UpdateEndOfShift(current);
                    row.TimeStamp = current;
                    log.Info($@"Closed Press - {row.MacName}");
                }

                //foreach (Press row in machineList)
                //{
                //    if (row.ActionCode == 4)
                //    {
                //        inst.EndOfTak(Array.FindIndex(Arr_Lbl, row1 => row1.Mach.Trim() == row.MacName.Trim()));
                //        // סגירת התקלה ופתיחתה מחדש לצורך הפרדה למשמרות
                //        row.UpdateEndOfShift(current);
                //        row.TimeStamp = current;
                //        log.Info($@"Closed Press - {row.MacName}");
                //    }
                //}
            }
        }


        private void ResizeMachiInfo()
        {
            int screenWidth = Screen.PrimaryScreen.Bounds.Width;
            int screenHeight = Screen.PrimaryScreen.Bounds.Height;
            int currentDPI = 0;
            using (Graphics g = this.CreateGraphics())
            {
                currentDPI = (int)g.DpiX;
            }
            if (currentDPI > 96)
            {
                foreach (var LBL in Arr_Lbl)
                {
                    LBL.ResizeLBL(1);
                }
            }
            if (screenWidth < 1920 && screenWidth > 1600)
            {
                foreach (var LBL in Arr_Lbl)
                {
                    LBL.ResizeLBL(1);
                }
            }
            else if (screenWidth <= 1600 && screenWidth > 1100)
            {
                foreach (var LBL in Arr_Lbl)
                {
                    LBL.ResizeLBL(2);
                }
            }
        }

        private void Fill_MachineList(DataTable DT)
        {
            machineList.Clear();
            foreach (DataRow row in DT.Rows)
            {
                if (machineList.Count > 0)
                {
                    var n = machineList.FirstOrDefault(m => m.Size == row[0].ToString().Trim());
                    if (n != null)
                        n.WorkCenter = 12;
                    else
                    {
                        //$@"SELECT spress, stimestamp,stak,sact,sstdtm,stur,semp,sprod,insiz,prati,family,MHSTT,'0'
                        //LPROD,INSIZ,LLBLNO,LACTAN,LACCPT,lacndt,lacntm 4,5...
                        var m1 = new Press();
                        m1.MacName = row["spress"].ToString().Trim(); // מכבש
                        m1.Department = row["sdept"].ToString(); // מחלקה
                        m1.TimeStamp = DateTime.ParseExact(row["stimestamp"].ToString().Trim().Substring(0, 19), "yyyy-MM-dd-HH.mm.ss", CultureInfo.InvariantCulture); // TimeStamp
                        m1.TakCode = int.Parse(row["stak"].ToString().Trim());
                        m1.ActionCode = int.Parse(row["sact"].ToString().Trim());
                        m1.Teken = row["sstdtm"].ToString() != "" ? int.Parse(row["sstdtm"].ToString().Trim()) : 0;
                        m1.Column = int.Parse(row["stur"].ToString().Trim());
                        m1.Oved = row["semp"].ToString().Trim();
                        m1.Item = row["sprod"].ToString().Trim();
                        m1.Size = row["insiz"].ToString().Trim();
                        m1.F_Name = row["prati"].ToString().Trim();
                        m1.L_Name = row["family"].ToString().Trim();
                        m1.Status = row["mhstt"].ToString().Trim();

                        machineList.Add(m1);
                    }
                }
                else
                {
                    //$@"SELECT spress, stimestamp,stak,sact,sstdtm,stur,semp,sprod,insiz,prati,family,MHSTT,'0'
                    //LPROD,INSIZ,LLBLNO,LACTAN,LACCPT,lacndt,lacntm 4,5...
                    var m1 = new Press();
                    m1.MacName = row["spress"].ToString().Trim(); // מכבש
                    m1.Department = row["sdept"].ToString(); // מחלקה
                    m1.TimeStamp = DateTime.ParseExact(row["stimestamp"].ToString().Trim().Substring(0, 19), "yyyy-MM-dd-HH.mm.ss", CultureInfo.InvariantCulture); // TimeStamp
                    m1.TakCode = int.Parse(row["stak"].ToString().Trim());
                    m1.ActionCode = int.Parse(row["sact"].ToString().Trim());
                    m1.Teken = row["sstdtm"].ToString() != "" ? int.Parse(row["sstdtm"].ToString().Trim()) : 0;
                    m1.Column = int.Parse(row["stur"].ToString().Trim());
                    m1.Oved = row["semp"].ToString().Trim();
                    m1.Item = row["sprod"].ToString().Trim();
                    m1.Size = row["insiz"].ToString().Trim();
                    m1.F_Name = row["prati"].ToString().Trim();
                    m1.L_Name = row["family"].ToString().Trim();
                    m1.Status = row["mhstt"].ToString().Trim();

                    machineList.Add(m1);
                }
            }
        }

        public void CreateLBL()
        {
            // שינוי יצירת לייבלים - יצירה ממערך המכונות
            for (i = 0; i < 60; i++)
            {
                Arr_Lbl[i] = new MachineInfo();
                Arr_Lbl[i].CuredTires(machineList[i].Item);
                Arr_Lbl[i].Mach = machineList[i].MacName;
                Arr_Lbl[i].Time = machineList[i].TimeStamp.ToString().Trim();
                Arr_Lbl[i].Oved = machineList[i].L_Name == "" ? "" : machineList[i].L_Name.Substring(0, 1) + " " + machineList[i].F_Name;
                Arr_Lbl[i].Status = machineList[i].ActionCode;
                //Arr_Lbl[i].BackColor = Color.LimeGreen;
                Arr_Lbl[i].Show();
                if (machineList[i].Column == 1)
                {
                    flowLayoutPanel2.Controls.Add(Arr_Lbl[i]);
                }
                else if (machineList[i].Column == 2)
                {
                    flowLayoutPanel3.Controls.Add(Arr_Lbl[i]);
                }
                else if (machineList[i].Column == 3)
                {
                    flowLayoutPanel4.Controls.Add(Arr_Lbl[i]);
                }
                else if (machineList[i].Column == 4)
                {
                    flowLayoutPanel5.Controls.Add(Arr_Lbl[i]);
                }
                else if (machineList[i].Column == 5)
                {
                    flowLayoutPanel6.Controls.Add(Arr_Lbl[i]);
                }
                else
                {
                    flowLayoutPanel7.Controls.Add(Arr_Lbl[i]);
                }
            }
        }

        public DataTable Initialize()
        {
            // אתחול מסך אפליקציה
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            StrSql = $@"SELECT spress,sdept, stimestamp,stak,sact, sstdtm,stur,semp,sprod,insiz,prati,family,MHSTT,'0'
                        FROM {Lib}.taxisttp 
                        LEFT JOIN BPCSFALI.IIMN on left(sprod,8)=INPROD 
                        LEFT JOIN BPCSFALI.FRTML01 ON left(SPROD,8) = RMPROD AND RMMACI = spress
                        LEFT JOIN BPCSFV30.LMHL01 a ON a.MHMACH = spress
                        LEFT OUTER JOIN isufkv.isavl10 on  ('00' ||semp) = oved 
                        LEFT JOIN TAPIALI.PRSSTTP b on  spress=b.MHMACH 
                        ORDER by sseq ";
            try
            {
                return DT = dbs.executeSelectQueryNoParam(StrSql);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw;
            }

        }
        #region
        private void Form1_SizeChanged(object sender, EventArgs e)
        {

        }

        #endregion
        private void Blink(MachineInfo m, int stat)
        {
            if (m.Blink)
            {
                switch (stat)
                {
                    case 1:
                        m.BlinkLBL(stat);
                        break;
                    case 2:
                        m.BlinkLBL(stat);
                        break;
                    case 3:
                        m.BlinkLBL(stat);
                        break;
                    case 4:
                        m.BlinkLBL(stat);
                        break;
                }
                m.InBlink = true;
            }
        }


        private void UpdateTables()
        {
            DBService dbs = new DBService();
            DataTable Result = new DataTable();
            Pl.Connect();
            if (Pl.Connected)
            {
                foreach (var item in machineList)
                {
                    //if (item.MacName == "421")
                    //{
                        // קריאת נתונים מבקר
                        if (item.MacName != "301" && item.MacName != "302" && item.MacName != "303" && item.MacName != "304" && item.MacName != "307" && item.MacName != "308" && item.MacName != "311" && item.MacName != "312" && item.MacName != "313" && item.MacName != "314" && item.MacName != "322" && item.MacName != "323")
                        {
                            if (item.MacName == "336")
                            {
                                //item.ReadPLC(); // Test
                            }
                            item.ReadPLC();
                        }
                        else if (item.MacName == "303" || item.MacName == "304" || item.MacName == "301" || item.MacName == "302" || item.MacName == "322" || item.MacName == "323" || item.MacName == "307" || item.MacName == "308" || item.MacName == "313" || item.MacName == "314" || item.MacName == "311" || item.MacName == "312")
                        {
                            if (item.MacName == "307" || item.MacName == "308")
                            {
                                item.ReadPLC_307_308();
                            }
                            else
                            {
                                item.ReadPLCDual();
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(item.Item) && !item.ReadingError) // מקט לא ריק ואין בעיה בקריאת הבקרים
                        {
                            var index = machineList.FindIndex(row1 => row1.MacName.ToString().Trim() == item.MacName.ToString().Trim());

                            //StrSql = $@"SELECT INSIZ,LEMP, lact,LTAK,ltimestamp,Lprod
                            //        FROM {Lib}.TAXILOGP
                            //        Left join BPCSFALI.IIMN on left(LPROD,8)=INPROD 
                            //        WHERE LPRESS={item.MacName}
                            //        ORDER by ltimestamp desc fetch first row only ";
                            //Result = dbs.executeSelectQueryNoParam(StrSql);
                            Result = item.GetLastRow();

                            if (Result.Rows.Count > 0)
                            {
                                // נבדוק אם מכבש בחימום
                                if (item.IsHeating && item.TakCode != 22)
                                {
                                    // דיווח התקלה
                                    UpdateStatus(item.MacName, "22", "0");
                                }
                                // בדיקה האם מכבש פתוח או סגור. במידה וסגור נבדוק אם המכבש במערך שלנו פתוח - אם כן  נעדכן.
                                //if (Result.Rows[0][4].ToString().Trim() == "41")
                                if (item.IsCuring && Result.Rows[0]["LACT"].ToString() != "1")
                                {
                                    #region
                                    //if (item.MacName == "336")
                                    //{
                                    //    // תחילת פיילוט - עצירת מכבש בכל פתיחה ב 336
                                    //    // שחרור העצירה!
                                    //    //item.Press_Stop(0);
                                    //}
                                    #endregion
                                    // בדיקה האם המכבש היה בתקלה ואם כן נסגור אותה ונעדכן את זמן התקלה
                                    if (item.ActionCode == 4)
                                    {
                                        // נסגור את התקלה האחרונה עם סך דקות התקלה
                                        EndOfTak(int.Parse(index.ToString()));
                                    }

                                    item.TimeStamp = DateTime.Now;
                                    item.Size = Result.Rows[0]["insiz"].ToString().Trim();
                                    item.ActionCode = 1;
                                    item.Oved = int.Parse(Result.Rows[0]["LEMP"].ToString().Trim()).ToString();
                                    item.TakCode = 0;

                                    item.IsCuring = item.IsCuring;
                                    item.IsHeating = item.IsHeating;


                                    if (IsServer)
                                    {
                                        ///// עדכון הטבלה במסד הנתונים
                                        item.UpdateTAXISTTP(true);

                                        ////עדכון טבלת הלוג
                                        item.InsertToLog();

                                        #region
                                        //StrSql = "update " + Lib + ".taxisttp set stimestamp='" + item.TimeStamp.ToString("yyyy-MM-dd-HH.mm.ss.00000") + "', " +
                                        //         "sact='" + item.ActionCode + "', semp='" + item.Oved + "', stak='" + item.TakCode + "',sprod='" + item.Item.PadRight(15, ' ').Substring(0, 15) + "', sstdtm='" + item.Teken + "' " +
                                        //         "WHERE spress='" + item.MacName + "' ";
                                        //dbs.executeInsertQuery(StrSql);

                                        //StrSql = "insert into " + Lib + ".taxilogp values('" + item.TimeStamp.ToString("yyyy-MM-dd-HH.mm.ss.00000") + "'," + item.Department.Trim() + ",'" + item.MacName.Trim() + "', " +
                                        //         "'" + item.ActionCode + "','" + item.Oved + "','" + item.TakCode + "', ' ','" + "server" + "','" + item.Item.PadRight(15, ' ').Substring(0, 15) + "','" + item.Teken + "') ";
                                        //dbs.executeInsertQuery(StrSql);
                                        #endregion
                                    }
                                }
                                // בדיקה האם מכבש פתוח או סגור. במידה ופתוח נבדוק אם המכבש במערך שלנו סגור - אם כן  נעדכן.
                                else if (!item.IsCuring && Result.Rows[0]["LACT"].ToString() == "1") // מכבש פתוח
                                {
                                    if (item.MacName == "336")
                                    {
                                        // תחילת פיילוט - עצירת מכבש בכל פתיחה ב 336
                                        // עצירת המכבש!
                                        //item.Press_Stop(1);
                                    }
                                    // אם קוד פעולה 4 - נבדוק אם הדיווח מהתוצאות עדכני יותר משאצלנו בטבלה או לא
                                    if (item.ActionCode == 4)
                                    {
                                        item.TimeStamp = DateTime.Now;
                                        item.Size = Result.Rows[0]["insiz"].ToString().Trim();
                                        item.ActionCode = 0;
                                        item.Oved = int.Parse(Result.Rows[0]["LEMP"].ToString().Trim()).ToString();
                                        item.TakCode = 0;

                                        item.IsCuring = item.IsCuring;
                                        item.IsHeating = item.IsHeating;

                                        if (IsServer)
                                        {
                                            ///// עדכון הטבלה במסד הנתונים
                                            item.UpdateTAXISTTP();

                                            ////עדכון טבלת הלוג
                                            item.InsertToLog();

                                            #region
                                            //StrSql = "update " + Lib + ".taxisttp set stimestamp='" + item.TimeStamp.ToString("yyyy-MM-dd-HH.mm.ss.00000") + "', " +
                                            //         "sact='" + item.ActionCode + "', semp='" + item.Oved + "', stak='" + item.TakCode + "',sprod='" + item.Item.PadRight(15, ' ').Substring(0, 15) + "'" +
                                            //         "WHERE  spress='" + item.MacName + "' ";
                                            //dbs.executeInsertQuery(StrSql);

                                            //StrSql = "insert into " + Lib + ".taxilogp values('" + item.TimeStamp.ToString("yyyy-MM-dd-HH.mm.ss.00000") + "'," + item.Department.Trim() + ",'" + item.MacName.Trim() + "', " +
                                            //"'" + item.ActionCode + "','" + item.Oved + "','" + item.TakCode + "', ' ','" + "server" + "','" + item.Item.PadRight(15, ' ').Substring(0, 15) + "','" + item.Teken + "') ";
                                            //dbs.executeInsertQuery(StrSql);
                                            #endregion
                                        }
                                    }
                                    //}
                                    else // בוצע גיפור בפועל - כל דיווחי סוף הגיפור כאן
                                    {
                                        item.TimeStamp = DateTime.Now;
                                        item.Size = Result.Rows[0]["insiz"].ToString().Trim();
                                        item.ActionCode = 0;
                                        item.Oved = int.Parse(Result.Rows[0]["LEMP"].ToString().Trim()).ToString();
                                        item.TakCode = 0;

                                        item.IsCuring = item.IsCuring;
                                        item.IsHeating = item.IsHeating;
                                        /// עדכון הטבלה במסד הנתונים
                                        if (IsServer)
                                        {
                                            /// דיווח 42 אוטומטי
                                            item.ReportPressOpen();

                                            item.UpdateTAXISTTP();

                                            ////עדכון טבלת הלוג
                                            item.InsertToLog();
                                            /// הוספת גיפור לתבנית 
                                            item.AddCureToMold();
                                            /// הוספת גיפור לבלדר
                                            item.AddCureToBladder();
                                        }
                                    }
                                }
                                // בדיקה האם סיים חימום - במידה וכן צריך לכתוב רשומה של מכבש פתוח
                                else if (!item.IsCuring && !item.IsHeating && Result.Rows[0]["LACT"].ToString() == "4" && Result.Rows[0]["LTAK"].ToString() == "22")
                                {
                                    item.TimeStamp = DateTime.Now;
                                    item.Size = Result.Rows[0]["insiz"].ToString().Trim();
                                    item.ActionCode = 0;
                                    item.Oved = int.Parse(Result.Rows[0]["LEMP"].ToString().Trim()).ToString();
                                    item.TakCode = 0;

                                    item.IsCuring = item.IsCuring;
                                    item.IsHeating = item.IsHeating;
                                    if (IsServer)
                                    {
                                        ///// עדכון הטבלה במסד הנתונים
                                        item.UpdateTAXISTTP();

                                        ////עדכון טבלת הלוג
                                        item.InsertToLog();
                                        //// עצירת המכבש 
                                        //Task.Run(() => Pl.Press_Stop(item, 0));
                                        #region

                                        //StrSql = "update " + Lib + ".taxisttp set stimestamp='" + item.TimeStamp.ToString("yyyy-MM-dd-HH.mm.ss.00000") + "', " +
                                        //         "sact='" + item.ActionCode + "', semp='" + item.Oved + "', stak='" + item.TakCode + "',sprod='" + item.Item.PadRight(15, ' ').Substring(0, 15) + "'" +
                                        //         "WHERE spress='" + item.MacName + "' ";
                                        //dbs.executeInsertQuery(StrSql);

                                        //StrSql = "insert into " + Lib + ".taxilogp values('" + item.TimeStamp.ToString("yyyy-MM-dd-HH.mm.ss.00000") + "'," + item.Department.Trim() + ",'" + item.MacName.Trim() + "', " +
                                        //"'" + item.ActionCode + "','" + item.Oved + "','" + item.TakCode + "', ' ','" + "server" + "','" + item.Item.PadRight(15, ' ').Substring(0, 15) + "','" + item.Teken + "') ";
                                        //dbs.executeInsertQuery(StrSql);
                                        #endregion
                                    }
                                }
                            }
                        }
                  //}  //טסטים//
                }
                // בדיקה האם צריך לנעול את פאנל הבקר
                List<Press> PanelsToClose = new List<Press>();
                Press P = new Press();
                PanelsToClose = P.GetPanelsToClose();
                foreach (Press row in PanelsToClose)
                {
                    Pl.ReleasePanelForEdit(row, 0);
                    row.UpdatePanelClosed();
                }
                Pl.Disconnect();

                // עדכון ריצה בלוג
                log.Info("Run Succeessfully");
            }
        }

        public int CallAS400TEKEN(string sql, string item, string mach)
        {
            DBService dbs = new DBService();
            DataTable DTSql = new DataTable();
            string str = $@"SELECT MHWRKC 
                            FROM bpcsfv30.lmhl01 
                            WHERE mhmach='" + mach + "' ";
            DT = dbs.executeSelectQueryNoParam(str);
            StrSql = "Call TAPIALI.TKNC4('" + item + "','F1','" + DT.Rows[0][0] + "','" + mach.PadRight(10, ' ') + "' ) ";
            dbs.executeInsertQuery(StrSql);
            DTSql = dbs.executeSelectQueryNoParam(sql);
            return Convert.ToInt32(Convert.ToDouble(DTSql.Rows[0][0]));
        }

        public static DateTime ConverToDT(string date, string time)
        {
            string TimeStamp;
            if (time.Length == 2)
            {
                TimeStamp = date.Substring(6, 2) + "/" + date.Substring(4, 2) + "/" + date.Substring(0, 4) + " 00:00:" + time;
            }
            else if (time.Length == 3)
            {
                TimeStamp = date.Substring(6, 2) + "/" + date.Substring(4, 2) + "/" + date.Substring(0, 4) + " 00:0" + time.Substring(0, 1) + ":" + time.Substring(1, 2);
            }
            else if (time.Length == 4)
            {
                TimeStamp = date.Substring(6, 2) + "/" + date.Substring(4, 2) + "/" + date.Substring(0, 4) + " 00:" + time.Substring(0, 2) + ":" + time.Substring(2, 2);
            }
            else if (time.Length == 5)
            {
                TimeStamp = date.Substring(6, 2) + "/" + date.Substring(4, 2) + "/" + date.Substring(0, 4) + " 0" + time.Substring(0, 1) + ":" + time.Substring(1, 2) + ":" + time.Substring(3, 2);
            }
            else
            {
                TimeStamp = date.Substring(6, 2) + "/" + date.Substring(4, 2) + "/" + date.Substring(0, 4) + " " + time.Substring(0, 2) + ":" + time.Substring(2, 2) + ":" + time.Substring(4, 2);
            }
            return DateTime.Parse(TimeStamp);
        }

        // טיימר לצביעת המכונות בצבעים השונים 
        private void timer2_Tick(object sender, EventArgs e)
        {
            TimerCount += 1;
            RefreshLBL();
        }

        public void RefreshLBL()
        {
            foreach (var row in machineList)
            {
                //if (row.MacName == "323")
                //{

                //}
                var index = Array.FindIndex(Arr_Lbl, row1 => row1.Mach.Trim() == row.MacName.Trim());
                // מכבש פתוח
                if (row.ActionCode == 0)
                {
                    DateTime OpenTime = Convert.ToDateTime(row.TimeStamp).AddMinutes(15);
                    Arr_Lbl[index].Blink = OpenTime < DateTime.Now ? true : false;
                    if (Arr_Lbl[index].Blink)
                    {
                        if (row.checkStt() == "5")
                        {
                            Blink(Arr_Lbl[index], 4);
                        }
                        else
                        {
                            Blink(Arr_Lbl[index], 1);
                        }
                    }
                    else
                    {
                        Arr_Lbl[index].SameColor();
                    }
                }
                // מכבש בגיפור
                else if (row.ActionCode == 1)
                {
                    DateTime GipurEnd = Convert.ToDateTime(row.TimeStamp).AddMinutes(double.Parse(row.Teken.ToString().Trim()) + 15);
                    //בלינקר
                    Arr_Lbl[index].Blink = GipurEnd < DateTime.Now ? true : false;
                    if (Arr_Lbl[index].Blink)
                    {
                        if (row.checkStt() == "5" || row.checkStt() == "6" || row.checkStt() == "7")
                        {
                            Blink(Arr_Lbl[index], 4);
                        }
                        else
                        {
                            Blink(Arr_Lbl[index], 2);
                        }
                    }
                    else
                    {
                        Arr_Lbl[index].SameColor();
                        Blink(Arr_Lbl[index], 2);
                    }
                }
                // מכבש בתקלה של חוסר צמיגים או חוסר תכנון 
                else if (row.ActionCode == 4 && (row.TakCode == 01 || row.TakCode == 69 || row.TakCode == 41))
                {
                    Blink(Arr_Lbl[index], 3);
                }
                else if (row.ActionCode == 4 && row.TakCode == 75)
                {
                    Blink(Arr_Lbl[index], 4);
                }
            }
        }

        // טיימר לעדכון כלל הטבלאות. רץ כל חצי דקה
        public void timer1_Tick(object sender, EventArgs e)
        {
            if (IsServer)
            {
                if (interval == 3)
                {
                    Fill_MachineList(Initialize()); // עדכון הנתונים מבסיס הנתונים - קריטי כדי לשמור על הכל מעודכן
                    UpdateTables();
                    Lbl_Update.Text = $"Last Update: {DateTime.Now.ToLongTimeString()}";
                }
            }
            if (interval == 3)
            {
                Fill_MachineList(Initialize()); // עדכון הנתונים מבסיס הנתונים
                ColorAllLBL();
                IsOpeningSoon();
                CheckForTires();
                Lbl_Update.Text = $"Last Update: {DateTime.Now.ToLongTimeString()}";

                // רענון נתוני ייצור בלייבל העליון
                foreach (MachineInfo mac in Arr_Lbl)
                {
                    var machine = machineList.FirstOrDefault(m => m.MacName.Trim() == mac.Mach.ToString().Trim());
                    mac.CuredTires(machine.Item);
                    mac.RefreshLBL(machine);
                }

                if (!IsServer)
                {
                    foreach (Press item in machineList)
                    {
                        // ?האם הגפר דיווח טעינה
                        // במידה ולא - נצבע את גבולות המכונה באדום כדי לסמן
                        var index = Array.FindIndex(Arr_Lbl, row1 => row1.Mach.Trim() == item.MacName.Trim());
                        if (item.ActionCode == 1 && !item.DidReadDNA())
                        {
                            Arr_Lbl[index].DNA_Unread_Color();
                        }
                        else
                        {
                            Arr_Lbl[index].DNA_Read_Color();
                        }
                    }
                }
                
                interval = 0;
                loggedIn = false;
            }
            else
            {
                interval++;
            }

        }

        public void ColorAllLBL()
        {
            // צביעת כלל הלייבלים
            foreach (var row in machineList)
            {
                DBService dbs = new DBService();
                DataTable DT = new DataTable();
                var index = Array.FindIndex(Arr_Lbl, row1 => row1.Mach.Trim() == row.MacName.Trim());
                Arr_Lbl[index].Blink = false;
                // עדכון הלייבלים 
                Arr_Lbl[index].Time = machineList[index].TimeStamp.ToString();
                Arr_Lbl[index].Oved = machineList[index].Oved;
                Arr_Lbl[index].TireSize = machineList[index].Size;
                Arr_Lbl[index].Status = machineList[index].ActionCode;
                if (row.ActionCode == 1)
                {
                    TimeSpan PressOpen = machineList[index].TimeStamp.AddMinutes(double.Parse(row.Teken.ToString().Trim())) - DateTime.Now;
                    Arr_Lbl[index].Counter = Math.Round(PressOpen.TotalMinutes).ToString();
                    Arr_Lbl[index].RefreshLBL(row);

                    // הוספת בדיקה האם המכבש שבגיפור הוא בסטטוס 5 - צמיג ראשון. אם כן, נצבע בהתאם
                    if (row.checkStt() == "5")
                    {
                        Arr_Lbl[index].TireSize = "צמיג ראשון בגיפור";
                        Arr_Lbl[index].BackColor = System.Drawing.Color.Yellow;
                        ///////// השמת צבע הרקע גם על הלייבל האמצעי - כדי לבטל את הצבע השונה בעת הבהוב  - כדי לגרום לצבע הרקע להיות אחיד
                        Arr_Lbl[index].TimeLBLColor(Arr_Lbl[index].BackColor);
                        Arr_Lbl[index].ForeColor = System.Drawing.Color.LimeGreen;
                    }
                    else if (!Arr_Lbl[index].Blink)
                    {
                        Arr_Lbl[index].BackColor = System.Drawing.Color.LimeGreen;
                        ///////// השמת צבע הרקע גם על הלייבל האמצעי - כדי לבטל את הצבע השונה בעת הבהוב  - כדי לגרום לצבע הרקע להיות אחיד
                        Arr_Lbl[index].TimeLBLColor(Arr_Lbl[index].BackColor);
                        /////////
                        Arr_Lbl[index].LblTextColor();
                    }
                }
                else
                {
                    Arr_Lbl[index].Counter = "";
                    if (row.ActionCode == 0)
                    {
                        // דיווח פריקה לאחר צמיג ראשון שהבהב. נבדוק האם הצמיג תקין
                        if (row.checkStt() == "5" || row.checkStt() == "6")
                        {
                            Arr_Lbl[index].Blink = false;
                            Arr_Lbl[index].BackColor = System.Drawing.Color.Yellow;
                            ///////// השמת צבע הרקע גם על הלייבל האמצעי - כדי לבטל את הצבע השונה בעת הבהוב  - כדי לגרום לצבע הרקע להיות אחיד
                            Arr_Lbl[index].TimeLBLColor(Arr_Lbl[index].BackColor);
                            Arr_Lbl[index].ForeColor = System.Drawing.Color.LimeGreen;
                            Arr_Lbl[index].TireSize = "צמיג ראשון בבדיקה";
                        }
                        // אם לא תקין
                        else
                        {
                            if (row.checkStt() == "7")
                            {
                                Arr_Lbl[index].Blink = false;
                                Arr_Lbl[index].BackColor = System.Drawing.Color.Yellow;
                                ///////// השמת צבע הרקע גם על הלייבל האמצעי - כדי לבטל את הצבע השונה בעת הבהוב  - כדי לגרום לצבע הרקע להיות אחיד
                                Arr_Lbl[index].TimeLBLColor(Arr_Lbl[index].BackColor);
                                Arr_Lbl[index].ForeColor = System.Drawing.Color.Red;
                                Arr_Lbl[index].TireSize = "צמיג ראשון לא תקין";
                            }
                            else if (!Arr_Lbl[index].Blink)
                            {
                                Arr_Lbl[index].BackColor = System.Drawing.Color.Red;
                                ///////// השמת צבע הרקע גם על הלייבל האמצעי - כדי לבטל את הצבע השונה בעת הבהוב  - כדי לגרום לצבע הרקע להיות אחיד
                                Arr_Lbl[index].TimeLBLColor(Arr_Lbl[index].BackColor);
                                Arr_Lbl[index].LblTextColor();
                            }
                        }
                    }

                    if (row.ActionCode == 4 && row.TakCode == 75)
                    {
                        Arr_Lbl[index].ChangeColor(row.TakCode.ToString());
                    }
                    else if (row.ActionCode == 4)
                    {
                        Arr_Lbl[index].ChangeColor(row.TakCode.ToString());
                        Arr_Lbl[index].TimeLBLForeColor(System.Drawing.Color.Black);
                    }
                    else if (!Arr_Lbl[index].Blink)
                    {
                        //Arr_Lbl[index].ChangeColor(row.TakCode.ToString());
                    }

                    //PRSSTTP עדכון המטריצה הפנימית בסטטוס מ 
                    Arr_Lbl[index].RefreshLBL(machineList[index]);
                }
            }

            Counter++;
            // בדיקת צמיגים בסטטוס חוסר תכנון או חוסר צמיגים
            if (Counter == 2)
            {
                CheckForTires();
                Counter = 0;
            }
        }

        // פונקציה המחזירה את מספר המשמרת בהינתן שעה
        public static string Shift(string time)
        {
            DBService dbs = new DBService();
            DataTable DT;
            string StrSql = $@"SELECT sshft 
                               FROM BPCSFALI.shf 
                               WHERE sday=0 and {time} BETWEEN sfhrs AND sthrs";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count == 0)
            {
                return "1";
            }
            else
            {
                return DT.Rows[0][0].ToString();
            }
        }

        // עדכונים מתאריך 29/05/2017
        // 
        public void UpdateStatus(string mach, string kod, string oved)
        {
            DBService dbs = new DBService();
            //DataTable DT1;
            //string StrSql = "";
            //bool lastKod = false;
            var index = Array.FindIndex(Arr_Lbl, row1 => row1.Mach.Trim() == mach);

            // שליפת סטטוס אחרון של המכבש פתיחה או סגירה
            //StrSql = $@"SELECT LACTAN 
            //            FROM tapiali.labelgp 
            //            WHERE LMACH = '{mach}' and LACTAN in (41,42) AND LERRCD = '' 
            //            ORDER by lacndt desc, lacntm desc fetch first row only ";
            //DT1 = dbs.executeSelectQueryNoParam(StrSql);
            //if (DT1.Rows.Count > 0)
            //{
            //    lastKod = DT1.Rows[0][0].ToString() == "42" && machineList[index].ActionCode != 4 ? true : false;
            //}
            //string stimeStamp = lastKod == true ? machineList[index].TimeStamp.AddSeconds(1).ToString("yyyy-MM-dd-HH.mm.ss.00000") : DateTime.Now.ToString("yyyy-MM-dd-HH.mm.ss.00000");

            // עדכון טבלת הלוג בכל תקלה. לפני העדכון נבדוק האם המכבש כבר בתקלה - אם כן, נסגור את התקלה ורק אז נפתח חדשה
            if (machineList[index].ActionCode == 4)
            {
                // נסגור את התקלה האחרונה עם סך דקות התקלה
                EndOfTak(int.Parse(index.ToString()));
                // נעדכן את המטריצה הפנימית בזמן החדש
                //machineList[index].TimeStamp = DateTime.Now;
            }

            machineList[index].TimeStamp = machineList[index].ActionCode == 0 ? machineList[index].TimeStamp.AddSeconds(1): DateTime.Now;

            // אם חימום נשים את זמן עכשיו
            if (kod == "22")
            {
                machineList[index].TimeStamp = DateTime.Now;
            }

            // עדכון המטריצה הפנימית
            machineList[index].TakCode = int.Parse(kod);
            machineList[index].Oved = oved;

            // אם עוד לא בתקלה נעדכן מכבש בתקלה ואז נעדכן את הלוג
            machineList[index].ActionCode = 4;

            // עדכון הטבלה עם קוד 4 כקוד פעולה
            machineList[index].UpdateTAXISTTP();
            // הוספת התקלה ללוג התנועות
            machineList[index].InsertToLog();

            // סגירת תקלה במידה והדיווח האחרון במשמרת קודמת
            DateTime lastRow = DateTime.ParseExact(machineList[index].GetLastRow().Rows[0]["ltimestamp"].ToString().Trim().Substring(0, 19), "yyyy-MM-dd-HH.mm.ss", CultureInfo.InvariantCulture);
            if (machineList[index].IsShiftOverLap(lastRow))
            {
                machineList[index].InsertEndShiftToLog(machineList[index].GetShiftStart());
            }

            //TAPIALI.PRSSTTP תחילת התייחסות לצמיג ראשון. אם מדווחים קוד 04, בנוסף לעדכון הטבלאות הרגיל נעדכן גם את 
            if (kod == "04")
            {
                machineList[index].UpdatePRSSTTP(3);
            }
            // שלב 2 צמיג ראשון: בדיקה האם כאשר מדווחים צמיג ראשון הסטטוס בטבלה הוא 3 או 5
            if (kod == "75")
            {
                string MHSTT = machineList[index].checkStt();
                if (MHSTT == "3" || MHSTT == "5" || MHSTT == "7")
                {
                    machineList[index].UpdatePRSSTTP(5);
                    // הבהוב שחור - צהוב
                    Arr_Lbl[index].Blink = true;
                    Blink(Arr_Lbl[index], 4);
                }
                else
                {
                    MessageBox.Show("שגיאה : ניתן לדווח טעינת צמיג ראשון רק אחרי דיווח על החלפת תבנית או החלפת כתובות");
                }
            }
            //// עדכון המטריצה 
            //machineList[index].ActionCode = 4;
            //machineList[index].TakCode = int.Parse(kod);
            // 
            //if (!lastKod)
            //    machineList[index].TimeStamp = DateTime.Now;
            Arr_Lbl[index].RefreshLBL(machineList[index]);
        }

        public void CheckForTires()
        {
            foreach (Press P in machineList)
            {
                DBService dbs = new DBService();
                DataTable DT, DT1;
                var index = Array.FindIndex(Arr_Lbl, row1 => P.MacName == row1.Mach);
                // אם מכבש בתקלה והתקלה היא חוסר צמיגים או חוסר תכנון 
                if (P.ActionCode == 4 && (P.TakCode == 1 || P.TakCode == 69 || P.TakCode == 41))
                {
                    string shft = Shift(DateTime.Now.ToString("HHmm"));
                    StrSql = $@"SELECT BCHLD,INSIZ 
                             FROM RZPALI.MCOVIL13
                             Join BPCSFV30.MBML01 on OPRIT=BPROD and BSEQ=1 
                             Left join BPCSFALI.IIMN on left(BCHLD,8)=INPROD 
                             WHERE ODATE={DateTime.Now.ToString("1yyMMdd")} and OSHIFT='{shft}' and OMACH='{P.MacName.Trim()}'
                             AND ODEPW in (161,162) and OPLAN>OMADE
                             FETCH first 1 rows only ";
                    DT = dbs.executeSelectQueryNoParam(StrSql);
                    if (DT.Rows.Count > 0)
                    {
                        StrSql = $@"SELECT b.LPROD,substring(b.LLBLNO,0) as LLBLNO,substring(b.LSTTDT,0) as LSTTDT,substring(b.LSTTTM,0) as LSTTTM,b.LPRODA,case WHEN a.lloc is null Then '0' else a.lloc END as Location,case when (lopb+lrct+ladju-lissu) is null THEN '0' else lopb+lrct+ladju-lissu END  as Inventory  
                                     FROM TAPIALI.LABELP as b 
                                     Left join bpcsfv30.ilil01 as a on b.LPROD = a.lprod and a.lwhs = 'AL' 
                                     WHERE b.LSTT='3' and b.LPROD='{DT.Rows[0]["BCHLD"]}' AND b.LSTTDT>= {DateTime.Now.AddDays(-1).ToString("yyyyMMdd")}
                                     ORDER by b.lsttdt,b.lstttm ";
                        DT1 = dbs.executeSelectQueryNoParam(StrSql);
                        if (DT1.Rows.Count > 0)
                        {
                            // בדיקה האם להבהב כי יש צמיג
                            Arr_Lbl[index].Blink = true;
                            Blink(Arr_Lbl[index], 3);
                        }
                        else
                        {
                            Arr_Lbl[index].InBlink = false;
                        }
                    }
                }
            }
        }

        private void EndOfShiftTimer_Tick(object sender, EventArgs e)
        {
            //// טיימר לעדכון וסגירת תקלות בסוף כל משמרת. התקלות הפתוחות ייסגרו,
            ////יעודכן לכל תקלה כמה זמן היא ארכה עד כה, והתקלות הפתוחות ייפתחו מחדש למשמרת העוקבת
            //string morning = new TimeSpan(6, 30, 0).ToString("hhmm");
            //string noon = new TimeSpan(15, 00, 0).ToString("hhmm");
            //string night = new TimeSpan(23, 30, 0).ToString("hhmm");
            //string now = DateTime.Now.TimeOfDay.ToString("hhmm");
            //DateTime current = DateTime.Now;

            //if (now == morning || now == noon || now == night)
            //{
            //    log.Info("Entered end of shift");
            //    foreach (Press row in machineList)
            //    {
            //        if (row.ActionCode == 4)
            //        {
            //            log.Info($@"Press In Action code = 4 - {row.MacName}");
            //        }
            //    }

            //    foreach (Press row in machineList)
            //    {
            //        if (row.ActionCode == 4)
            //        {           
            //            EndOfTak(Array.FindIndex(Arr_Lbl, row1 => row1.Mach.Trim() == row.MacName.Trim()));
            //            // סגירת התקלה ופתיחתה מחדש לצורך הפרדה למשמרות
            //            row.UpdateEndOfShift(current);
            //            row.TimeStamp = current;
            //            log.Info($@"Closed Press - {row.MacName}");
            //        }
            //    }
            //}
        }

        public void EndOfTak(int index)
        {
            DBService dbs = new DBService();
            double TakTime = Math.Round((DateTime.Now - machineList[index].TimeStamp).TotalMinutes, 0);
            // נסגור את התקלה האחרונה עם סך דקות התקלה
            StrSql = $@"UPDATE {Lib1}.taxilogp
                        SET LSTDTM={TakTime}
                        WHERE LPRESS='{machineList[index].MacName}' and LACT='4' and LSTDTM=0 AND
                        LTIMESTAMP=(select max(ltimestamp) 
                            FROM {Lib1}.taxilogp 
                            WHERE LPRESS='{machineList[index].MacName}') ";
            dbs.executeInsertQuery(StrSql);
        }



        public void GetPLCAddresses()
        {
            DbServiceSQL dbsql = new DbServiceSQL();
            StrSql = $@"SELECT * 
                        FROM PLC_Registers ";
            DataTable DT = dbsql.GetDataSetByQuery(StrSql, CommandType.Text).Tables[0];
            foreach (DataRow row in DT.Rows)
            {
                Press P = new Press(row["Press"].ToString());
                P.CuringBit = !string.IsNullOrEmpty(row["Curing_Bit"] as string) ? row["Curing_Bit"].ToString() : null;
                P.HeatingBit = !string.IsNullOrEmpty(row["Heating_Bit"] as string) ? row["Heating_Bit"].ToString() : null;
                P.InnerPressureBit = !string.IsNullOrEmpty(row["InnerPressure_Bit"] as string) ? row["InnerPressure_Bit"].ToString() : null;
                P.Item = !string.IsNullOrEmpty(row["Cat_Number"] as string) ? row["Cat_Number"].ToString() : null;
                P.IP = !string.IsNullOrEmpty(row["IP"] as string) ? row["IP"].ToString() : null;
                P.CuringCycleBit = !string.IsNullOrEmpty(row["Curing_Cycle"] as string) ? row["Curing_Cycle"].ToString() : null;
                P.PanelLockBit = !string.IsNullOrEmpty(row["Panel_Lock"] as string) ? row["Panel_Lock"].ToString() : null;
                PressRegisters.Add(P);
            }
        }

        /// <summary>
        /// פונקציה לבדיקה וסימון של שורות מכבשים שעומדים להיפתח בקרוב
        /// </summary>
        public void IsOpeningSoon()
        {
            foreach (Control item in tableLayoutPanel1.Controls)
            {
                int count = 0;
                foreach (Control macInfo in item.Controls)
                {
                    if (macInfo.GetType() == typeof(MachineInfo))
                    {
                        MachineInfo M = macInfo as MachineInfo;
                        if (M.Counter != "")
                        {
                            if (double.Parse(M.Counter) < 10)
                            {
                                count++;
                            }
                        }
                    }
                }

                if (count >= 3)
                {
                    item.BackColor = Color.PaleGoldenrod;
                }
                else
                {
                    item.BackColor = Color.LightSlateGray;
                }
            }
        }



        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 Abox = new AboutBox1();
            Abox.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void gipurReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GipurReport G = new GipurReport();
            G.ShowDialog();
        }

        private void efficiencyReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GipurEfficiency G = new GipurEfficiency();
            G.ShowDialog();
        }

        private void flowLayoutPanel7_Paint(object sender, PaintEventArgs e)
        {

        }
        //RSLINX ננתק את החיבור לשרת ה 
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsServer)
            {
                Pl.Disconnect();
            }
        }
    }
}
