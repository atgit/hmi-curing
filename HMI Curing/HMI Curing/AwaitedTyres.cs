﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HMI_Curing
{
    public partial class AwaitedTyres : Form
    {
        DataTable info;
        public string Press { get; set; }
        public string Size { get; set; }

        public string Inventory { get; set; }
        public AwaitedTyres()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        public AwaitedTyres(DataTable DT, string press, string size, string invent)
        {
            info = DT;
            Press = press;
            Size = size;
            Inventory = invent;
            InitializeComponent();
        }
        // הזנת הנתונים לגריד
        private void AwaitedTyres_Load(object sender, EventArgs e)
        {
            //dataGridView1.DataSource = info;
            //dataGridView1.AutoGenerateColumns = false;
            if (info.Rows.Count > 0)
            {
                MachLBL.Text = "Press:  " + Press + ",   Size:  " + Size + ",   Inventory:   " + Inventory; //info.Rows[0][6].ToString();
            }
            else
            {
                MachLBL.Text = "Press:  " + Press + ",   Size:  " + Size;
            }

            dataGridView1.DataSource = info;
            // עיצוב גריד והוספת נתונים
            dataGridView1.Columns[0].DataPropertyName = "LPROD";
            dataGridView1.Columns[0].HeaderText = "Product";
            dataGridView1.Columns[0].HeaderCell.Style.Font = new Font("Tahoma", 10);
            dataGridView1.Columns[1].HeaderText = "Serial Number";
            dataGridView1.Columns[1].HeaderCell.Style.Font = new Font("Tahoma", 10);
            dataGridView1.Columns[2].DataPropertyName = "LSTTDT";
            dataGridView1.Columns[2].HeaderText = "Date";
            dataGridView1.Columns[2].HeaderCell.Style.Font = new Font("Tahoma", 10);
            dataGridView1.Columns[2].DefaultCellStyle.Format = "dd/MM/yyyy";
            dataGridView1.Columns[3].DataPropertyName = "LSTTTM";
            dataGridView1.Columns[3].HeaderText = "Time";
            dataGridView1.Columns[3].HeaderCell.Style.Font = new Font("Tahoma", 10);
            dataGridView1.Columns[3].DefaultCellStyle.Format = "HH:mm:ss";
            dataGridView1.Columns[4].DataPropertyName = "LPRODA";
            dataGridView1.Columns[4].HeaderText = "Building Machine";
            dataGridView1.Columns[4].HeaderCell.Style.Font = new Font("Tahoma", 10);
            dataGridView1.Columns[5].DataPropertyName = "Location";
            dataGridView1.Columns[5].HeaderText = "Location";
            dataGridView1.Columns[5].HeaderCell.Style.Font = new Font("Tahoma", 10);
            dataGridView1.Columns[6].Visible = false;
            //dataGridView1.Columns[6].DataPropertyName = "Inventory";
            dataGridView1.DefaultCellStyle.Font = new Font("Tahoma", 14);
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
          
            dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.AllowUserToAddRows = false;
            for (int n = 0; n < info.Rows.Count; n++)
            {
                // המרת הנתון לתאריך תקני
                DateTime G = Form1.ConverToDT(dataGridView1.Rows[n].Cells[2].Value.ToString(), dataGridView1.Rows[n].Cells[3].Value.ToString());
                dataGridView1.Rows[n].Cells[2].Value = G.ToString("dd/MM/yyyy");
                dataGridView1.Rows[n].Cells[3].Value = G.ToString("HH:mm:ss");
                if (G.AddHours(2) > DateTime.Now)
                {
                    dataGridView1.Rows[n].DefaultCellStyle.BackColor = Color.LimeGreen;
                }
            }

        }
        // הדפסת הצמיגים המוכנים 
        private void PringBTN_Click(object sender, EventArgs e)
        {
            PrintDocument pd = new PrintDocument();
            pd.PrintPage += new PrintPageEventHandler(PrintImage);
            pd.Print();
        }

        void PrintImage(object o, PrintPageEventArgs e)
        {
            int x = SystemInformation.WorkingArea.X;
            int y = SystemInformation.WorkingArea.Y;
            int width = this.Width;
            int height = this.Height;

            Rectangle bounds = new Rectangle(x, y, width, height);

            Bitmap img = new Bitmap(width, height);

            this.DrawToBitmap(img, bounds);
            Point p = new Point(100, 100);
            e.Graphics.DrawImage(img, p);
        }

        private void OKBTN_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
