﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HMI_Curing
{
    public partial class Info : Form
    {
        string tire;
        string size;
        Press p;

        public string Tire
        {
            get
            {
                return tire;
            }

            set
            {
                tire = value;
            }
        }

        public string Size1
        {
            get
            {
                return size;
            }

            set
            {
                size = value;
            }
        }

        public Press Machine
        {
            get
            {
                return p;
            }

            set
            {
                p = value;
            }
        }

        public Info()
        {
            InitializeComponent();
        }

        public Info(Press mach)
        {
            Tire = mach.Item;
            Size1 = mach.Size;
            Machine = mach;
            InitializeComponent();
        }

        private void Info_Load(object sender, EventArgs e)
        {
            GreenTire();
        }

        public void GreenTire()
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            string StrSql = "";
            string currentTire = Machine.CurrentCuringTire();
            // נקרא לפונקציה שתבדוק את המלאי הנוכחי למקט
            currentTire = Machine.GetGreenTireFromFinal(currentTire);
            StrSql = $@"Call bpcsoali.in@372C('{currentTire.PadRight(15,' ')}') ";
            dbs.executeInsertQuery(StrSql);
            StrSql = $@"SELECT ZPROD,ZQTY 
                        FROM bpcsfali.in@372F ";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            Lbl_Green.Text += "    " + DT.Rows[0][1].ToString();
            Lbl_Teken.Text += "    " + Machine.Teken;
            Lbl_Item.Text += "    " + Tire;
            Lbl_Size.Text += "    " + Size1;
           

        }
        private void Btn_OK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
