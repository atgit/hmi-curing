﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using ClassLibrary;

namespace HMI_Curing
{
    public partial class MachineInfo : UserControl
    {
        string StrSql = "";
        string tiresize = "";
        string time = "";

        public MachineInfo()
        {
            InitializeComponent();
        }

        public string Time
        {
            get { return time; }
            set { time = value; }
        }
        public string Mach { get; set; }
        public string Oved { get; set; }
        public string TireSize
        {
            get
            {
                return tiresize;
            }
            set
            {
                tiresize = value == null ? "" : value;
            }
        }
        public string Counter
        {
            get
            {
                return Lbl_Counter.Text;
            }
            set
            {
                Lbl_Counter.Text = value == null ? "" : value;
            }
        }
        public int Status { get; set; }
        public bool Blink { get; set; }
        public bool InBlink { get; set; }
        public int made { get; set; }
        public int planned { get; set; }
        private Form1 f;
        public System.Drawing.Color MachColor
        {
            get
            {
                return NameLBL.ForeColor;
            }
            set
            {
                NameLBL.ForeColor = value;
            }
        }

        public void ResizeLBL(int Size)
        {
            if (Size == 1)
            {
                this.Height = 60;
                this.Width = 270;
                NameLBL.Height = 19;
                SizeLBL.Height = 19;
                TimeLBL.Height = 19;
                NameLBL.Font = new Font("Tahoma", 11, FontStyle.Bold);
                SizeLBL.Font = new Font("Tahoma", 11, FontStyle.Bold);
                TimeLBL.Font = new Font("Tahoma", 11, FontStyle.Bold);
            }
            else if (Size == 2)
            {
                this.Height = 38;
                this.Width = 250;
                NameLBL.Height = 13;
                SizeLBL.Height = 13;
                TimeLBL.Height = 13;
                NameLBL.Font = new Font("Tahoma", 9, FontStyle.Bold);
                SizeLBL.Font = new Font("Tahoma", 9, FontStyle.Bold);
                TimeLBL.Font = new Font("Tahoma", 9, FontStyle.Bold);
            }
        }

        public void TimeLBLColor(System.Drawing.Color C)
        {
            TimeLBL.BackColor = C;
            //TimeLBL.ForeColor = NameLBL.ForeColor;
        }

        public void TimeLBLForeColor(System.Drawing.Color C)
        {
            TimeLBL.ForeColor = C;
        }

        public void DNA_Unread_Color()
        {
            Lbl_bottom.BackColor = Color.Red;
            Lbl_Top.BackColor = Color.Red;
        }
        public void DNA_Read_Color()
        {
            Lbl_bottom.BackColor = Color.Transparent;
            Lbl_Top.BackColor = Color.Transparent;
        }

        private void MachineInfo_Load(object sender, EventArgs e)
        {
            var machine = Form1.machineList.FirstOrDefault(m => m.MacName.Trim() == Mach.ToString().Trim());
            CuredTires(machine.Item);

            NameLBL.Text = Mach + " // " + planned + " // " + made;
            TimeLBL.Text = Time;
            //OvedLBL.Text = Oved;

        }

        public void LblTextColor()
        {
            foreach (Label lbl in this.Controls)
            {
                lbl.ForeColor = this.BackColor == Color.Black ? Color.White : Color.Black;
                if (this.TireSize == "צמיג ראשון בגיפור")
                {
                    lbl.ForeColor = System.Drawing.Color.LimeGreen;
                }
                else if (this.TireSize == "צמיג ראשון בבדיקה")
                {
                    lbl.ForeColor = System.Drawing.Color.Black;
                }
           }
        }

        public void BlinkLBL(int stat)
        {
            var main = Form1.inst;
            switch (stat)
            {
                case 1:
                    if (main.TimerCount % 2 == 0)
                    {
                        //TimeLBL.BackColor = TimeLBL.BackColor == System.Drawing.Color.Black ? System.Drawing.Color.IndianRed : System.Drawing.Color.Black;
                        //TimeLBL.ForeColor = TimeLBL.ForeColor == System.Drawing.Color.Black ? System.Drawing.Color.White : System.Drawing.Color.Black;    
                        //TimeLBL.BackColor = System.Drawing.Color.Black;
                        //TimeLBL.ForeColor = System.Drawing.Color.White;
                    }
                    else
                    {
                        //TimeLBL.BackColor = TimeLBL.BackColor == System.Drawing.Color.IndianRed ? System.Drawing.Color.Black : System.Drawing.Color.IndianRed;
                        //TimeLBL.ForeColor = TimeLBL.ForeColor == System.Drawing.Color.White ? System.Drawing.Color.Black : System.Drawing.Color.White;    
                        //TimeLBL.BackColor = System.Drawing.Color.IndianRed;
                        //TimeLBL.ForeColor = System.Drawing.Color.Black;
                    }

                    break;
                case 2:
                    if (main.TimerCount % 2 == 0)
                    {
                        //TimeLBL.BackColor = TimeLBL.BackColor == System.Drawing.Color.Black ? System.Drawing.Color.LimeGreen : System.Drawing.Color.Black;
                        //TimeLBL.ForeColor = TimeLBL.ForeColor == System.Drawing.Color.Black ? System.Drawing.Color.White : System.Drawing.Color.Black;
                        //TimeLBL.BackColor = System.Drawing.Color.Black;
                        //TimeLBL.ForeColor = System.Drawing.Color.White;
                    }
                    else
                    {
                        //TimeLBL.BackColor = TimeLBL.BackColor == System.Drawing.Color.LimeGreen ? System.Drawing.Color.Black : System.Drawing.Color.LimeGreen;
                        //TimeLBL.ForeColor = TimeLBL.ForeColor == System.Drawing.Color.White ? System.Drawing.Color.Black : System.Drawing.Color.White;
                        //TimeLBL.BackColor = System.Drawing.Color.LimeGreen;
                        //TimeLBL.ForeColor = System.Drawing.Color.Black;
                    }
                    break;
                case 3:
                    if (main.TimerCount % 2 == 0)
                    {
                        //TimeLBL.BackColor = TimeLBL.BackColor == System.Drawing.Color.Aqua ? System.Drawing.Color.Fuchsia : System.Drawing.Color.Aqua;
                        //TimeLBL.ForeColor = TimeLBL.ForeColor == System.Drawing.Color.Black ? System.Drawing.Color.White : System.Drawing.Color.Black;
                        TimeLBL.BackColor = System.Drawing.Color.Fuchsia;
                        TimeLBL.ForeColor = System.Drawing.Color.Black;
                    }
                    else
                    {
                        //TimeLBL.BackColor = TimeLBL.BackColor == System.Drawing.Color.Fuchsia ? System.Drawing.Color.Aqua : System.Drawing.Color.Fuchsia;
                        //TimeLBL.ForeColor = TimeLBL.ForeColor == System.Drawing.Color.White ? System.Drawing.Color.Black : System.Drawing.Color.White;
                        TimeLBL.BackColor = System.Drawing.Color.Aqua;
                        TimeLBL.ForeColor = System.Drawing.Color.Black;
                    }
                    break;
                case 4:
                    if (main.TimerCount % 2 == 0)
                    {
                        //TimeLBL.BackColor = TimeLBL.BackColor == System.Drawing.Color.Black ? System.Drawing.Color.Yellow : System.Drawing.Color.Black;
                        //TimeLBL.ForeColor = TimeLBL.ForeColor == System.Drawing.Color.Black ? System.Drawing.Color.White : System.Drawing.Color.Black;
                        TimeLBL.BackColor = System.Drawing.Color.Black;
                        TimeLBL.ForeColor = System.Drawing.Color.White;
                    }
                    else
                    {
                        //TimeLBL.BackColor = TimeLBL.BackColor == System.Drawing.Color.Yellow ? System.Drawing.Color.Black : System.Drawing.Color.Yellow;
                        //TimeLBL.ForeColor = TimeLBL.ForeColor == System.Drawing.Color.White ? System.Drawing.Color.Black : System.Drawing.Color.White;
                        TimeLBL.BackColor = System.Drawing.Color.Yellow;
                        TimeLBL.ForeColor = System.Drawing.Color.LimeGreen;
                    }
                    break;
            }
        }


        public MachineInfo(Form1 form)
        {
            f = form;
        }

        private void MachineInfo_DoubleClick(object sender, EventArgs e)
        {
            var machine = Form1.machineList.FirstOrDefault(m => m.MacName.Trim() == Mach.ToString().Trim());
            Login l = new Login(this);
            l.ID = Mach;
            l.macName = machine.MacName;
            l.Activate();
            l.ShowDialog();
        }

        public void ChangeColor(string kod)
        {
            if (kod == "9" || kod == "16" || kod == "38")
            {
                BackColor = System.Drawing.Color.White;
                TimeLBLColor(BackColor);
            }
            else if (kod == "12" || kod == "17" || kod == "18" || kod == "19" || kod == "20" || kod == "23")
            {
                BackColor = System.Drawing.Color.RoyalBlue;
                TimeLBLColor(BackColor);
            }
            else if (kod == "2" || kod == "8" || kod == "68" || kod == "70")
            {
                BackColor = System.Drawing.Color.FromArgb(255, 153, 204);
                TimeLBLColor(BackColor);
            }
            else if (kod == "4" || kod == "6" || kod == "75")
            {
                BackColor = System.Drawing.Color.Yellow;
                //BackColor = System.Drawing.Color.FromArgb(221,221,0);
                TimeLBLColor(BackColor);
            }
            else if (kod == "22")
            {
                BackColor = System.Drawing.Color.Orange;
                //BackColor = System.Drawing.Color.FromArgb(221,221,0);
                TimeLBLColor(BackColor);
            }
            else
            {
                BackColor = System.Drawing.Color.Cyan;
                TimeLBLColor(BackColor);
            }
        }

        public void SameColor()
        {
            TimeLBLColor(NameLBL.BackColor);
        }

        public void UpdateTbls(string kod, string name, string worker)
        {
            var f = Form1.inst;

            f.UpdateStatus(Mach, kod, name);
            TimeLBLColor(System.Drawing.Color.Black);
            UpdateStat(kod, name, worker);
        }

        public void UpdateStat(string kod, string name, string worker)
        {
            //NameLBL.Text = Mach + "    " + worker;
            NameLBL.Text = Mach + " // " + planned + " // " + made;
            SizeLBL.Text = GetTakDesc(kod);
            ChangeColor(kod);
        }

        // פונקציה לרענן את כיתוב הלייבל בעדכון
        public void RefreshLBL(Press P)
        {
            if (this.Status == 4)
            {
                NameLBL.Text = P.L_Name.Length > 0 ? P.MacName + "    " + P.L_Name.Substring(0, 1) + "." + P.F_Name : P.MacName;
                //NameLBL.Text = P.MacName + " // " + planned + " // " + made;
                SizeLBL.Text = this.GetTakDesc(P.TakCode.ToString());
            }
            else
            {
                NameLBL.Text = Mach + " // " + planned + " // " + made;
                SizeLBL.Text = TireSize.ToString();
            }
            TimeLBL.Text = P.TimeStamp.ToString();
            Lbl_Counter.Text = Counter;
        }

        public string GetTakDesc(string TakCod)
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            TakCod = TakCod.ToString().Length < 2 ? "0" + TakCod.ToString() : TakCod;
            string StrSql = $@"SELECT substring(IPROD,8,2)||' - '||trim(IDESC) as PR, substring(IPROD,8,2) as KOD 
                               FROM BPCSFV30.IIML01 
                               WHERE IPROD = 'TAK-161{TakCod}'";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                return DT.Rows[0][0].ToString();
            }
            else
            {
                return "";
            }
        }

        private void NameLBL_DoubleClick(object sender, EventArgs e)
        {
            MachineInfo_DoubleClick(sender, e);
        }

        private void TimeLBL_DoubleClick(object sender, EventArgs e)
        {
            MachineInfo_DoubleClick(sender, e);
        }

        private void SizeLBL_DoubleClick(object sender, EventArgs e)
        {
            MachineInfo_DoubleClick(sender, e);
        }

        private void NameLBL_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                //CheckTires();
                new Task(() => { CheckTires(); }).Start();
            }

        }
        private void TimeLBL_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                //CheckTires();
                new Task(() => { CheckTires(); }).Start();
            }
        }

        private void SizeLBL_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                //CheckTires();
                new Task(() => { CheckTires(); }).Start();
            }
        }
        // בדיקה האם למכבש הנמצא בתקלה יש צמיגים שגופרו ויכולים להיכנס
        public void CheckTires()
        {
            DBService dbs = new DBService();
            DataTable DT, DT1;
            string StrSql = "";
            var machine = Form1.machineList.FirstOrDefault(m => m.MacName.Trim() == Mach.ToString().Trim());
            // אם מכבש בתקלה והתקלה היא חוסר צמיגים או חוסר תכנון או מלאי שגוי 
            if (machine.ActionCode == 4 && (machine.TakCode == 1 || machine.TakCode == 69 || machine.TakCode == 41))
            {
                Press p = new Press(machine.MacName);
                string currentTire = p.CurrentCuringTire();
                if (currentTire != "STOP")
                {
                    try
                    {
                        // נקרא לפונקציה שתבדוק את המלאי הנוכחי למקט
                        currentTire = p.GetGreenTireFromFinal(currentTire);
                        string shft = Form1.Shift(DateTime.Now.ToString("HHmm"));
                        // נככב את הבדיקות של אלי - נעבור לבדיקות שמבוצעות כשנפתח החלון - בדיקת שקילות
                        StrSql = $@"SELECT BCHLD,INSIZ 
                                    FROM RZPALI.MCOVIL13 
                                    join BPCSFV30.MBML01 on OPRIT=BPROD and BSEQ=1 
                                    left join BPCSFALI.IIMN on left(BCHLD,8)=INPROD 
                               WHERE ODATE={DateTime.Now.ToString("1yyMMdd")} and OSHIFT='{shft}' and OMACH='{p.MacName.Trim()}' AND ODEPW in (161,162) and OPLAN>OMADE 
                                    FETCH first 1 rows only ";
                        DT = dbs.executeSelectQueryNoParam(StrSql);
                            StrSql = $@"SELECT b.LPROD,substring(b.LLBLNO,0) as LLBLNO,substring(b.LSTTDT,0) as LSTTDT,substring(b.LSTTTM,0) as LSTTTM,b.LPRODA,case WHEN a.lloc is null Then '0' else a.lloc END as Location,case when (lopb+lrct+ladju-lissu) is null THEN '0' else lopb+lrct+ladju-lissu END  as Inventory
                                     FROM TAPIALI.LABELP as b 
                                     Left join bpcsfv30.ilil01 as a on b.LPROD = a.lprod and a.lwhs = 'AL'
                                     WHERE b.LSTT='3' and b.LPROD='{DT.Rows[0][0]}' AND b.LSTTDT>={DateTime.Now.AddDays(-1).ToString("yyyyMMdd")}
                                     ORDER by b.lsttdt,b.lstttm ";

                            DT1 = dbs.executeSelectQueryNoParam(StrSql);
                            string inventory = DT1.Rows.Count > 0 ? DT1.Rows[0]["INVENTORY"].ToString() : "0";
                            AwaitedTyres At = new AwaitedTyres(DT1, machine.MacName, DT.Rows[0]["INSIZ"].ToString(), inventory);
                            At.Activate();
                            At.ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "CheckTires()");
                    }
                  
                }
                // אם לא אחת מהתקלות הנ"ל, נציג את הצמיג האחרון שהיה במכבש
                else
                {
                    machine.Size = currentTire;
                    Info I = new Info(machine);
                    I.Activate();
                    I.ShowDialog();
                }
            }
            // אם לא אחת מהתקלות הנ"ל, נציג את הצמיג האחרון שהיה במכבש
            else
            {
                //MessageBox.Show("Last tire size:  " + TireSize);
                Info I = new Info(machine);
                I.Activate();
                I.ShowDialog();
            }
        }

        public void CuredTires(string item)
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();

            string shift = "";
            string Date = DateTime.Now.ToString();

            Date = "1" + Date.Substring(8, 2) + Date.Substring(3, 2) + Date.Substring(0, 2);

            string now = DateTime.Now.ToString("HHmm");

            shift = Form1.Shift(now);

            if (!string.IsNullOrWhiteSpace(item) && item.Length > 7)
            {
                StrSql = $@"SELECT sum(OMADE),sum(OPLAN) 
                            FROM RZPALI.MCOVIP 
                            WHERE ODATE={Date} AND OMACH='{Mach}' AND OSHIFT='{shift}' AND substring(OPRIT,1,8)='{item.Substring(0, 8).Trim()}' AND ODEPW in (161,162) ";
                DT = dbs.executeSelectQueryNoParam(StrSql);

                if (DT.Rows.Count > 0)
                {
                    made = DT.Rows[0][0].ToString() != "" ? Convert.ToInt32(DT.Rows[0][0]) : 0;
                    planned = DT.Rows[0][1].ToString() != "" ? Convert.ToInt32(DT.Rows[0][1]) : 0;
                }
            }
        }
    }
}
