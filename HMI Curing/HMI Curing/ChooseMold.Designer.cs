﻿namespace HMI_Curing
{
    partial class ChooseMold
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DGV_Molds = new System.Windows.Forms.DataGridView();
            this.CB_Items = new System.Windows.Forms.ComboBox();
            this.Lbl_Title = new System.Windows.Forms.Label();
            this.Btn_Save = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Molds)).BeginInit();
            this.SuspendLayout();
            // 
            // DGV_Molds
            // 
            this.DGV_Molds.AllowUserToAddRows = false;
            this.DGV_Molds.AllowUserToDeleteRows = false;
            this.DGV_Molds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV_Molds.DefaultCellStyle = dataGridViewCellStyle3;
            this.DGV_Molds.Location = new System.Drawing.Point(32, 112);
            this.DGV_Molds.Name = "DGV_Molds";
            this.DGV_Molds.ReadOnly = true;
            this.DGV_Molds.Size = new System.Drawing.Size(1163, 97);
            this.DGV_Molds.TabIndex = 1;
            this.DGV_Molds.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.DGV_Molds_DataBindingComplete);
            // 
            // CB_Items
            // 
            this.CB_Items.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_Items.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CB_Items.FormattingEnabled = true;
            this.CB_Items.Location = new System.Drawing.Point(484, 79);
            this.CB_Items.Name = "CB_Items";
            this.CB_Items.Size = new System.Drawing.Size(291, 27);
            this.CB_Items.TabIndex = 2;
            this.CB_Items.SelectedIndexChanged += new System.EventHandler(this.CB_Items_SelectedIndexChanged);
            this.CB_Items.Click += new System.EventHandler(this.CB_Items_Click);
            // 
            // Lbl_Title
            // 
            this.Lbl_Title.AutoSize = true;
            this.Lbl_Title.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_Title.Location = new System.Drawing.Point(472, 23);
            this.Lbl_Title.Name = "Lbl_Title";
            this.Lbl_Title.Size = new System.Drawing.Size(0, 29);
            this.Lbl_Title.TabIndex = 3;
            // 
            // Btn_Save
            // 
            this.Btn_Save.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Btn_Save.Location = new System.Drawing.Point(561, 215);
            this.Btn_Save.Name = "Btn_Save";
            this.Btn_Save.Size = new System.Drawing.Size(136, 33);
            this.Btn_Save.TabIndex = 4;
            this.Btn_Save.Text = "בחר";
            this.Btn_Save.UseVisualStyleBackColor = true;
            this.Btn_Save.Click += new System.EventHandler(this.Btn_Save_Click);
            // 
            // ChooseMold
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1247, 264);
            this.Controls.Add(this.Btn_Save);
            this.Controls.Add(this.Lbl_Title);
            this.Controls.Add(this.CB_Items);
            this.Controls.Add(this.DGV_Molds);
            this.Name = "ChooseMold";
            this.Text = "בחירת תבנית";
            this.Load += new System.EventHandler(this.ChooseMold_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Molds)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView DGV_Molds;
        private System.Windows.Forms.ComboBox CB_Items;
        private System.Windows.Forms.Label Lbl_Title;
        private System.Windows.Forms.Button Btn_Save;
    }
}