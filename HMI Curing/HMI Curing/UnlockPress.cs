﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using ClassLibrary;

namespace HMI_Curing
{
    public partial class UnlockPress : Form
    {
        public int VerCode { get; private set; }
        public Press Mach { get; set; }

        public UnlockPress(Press mach)
        {
            Mach = mach;
            InitializeComponent();
            Send_VerifSMS();
            Txt_Verification.Focus();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            if (int.Parse(Txt_Verification.Text) == VerCode)
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
            }
            else
            {
                MessageBox.Show("הקוד אינו נכון, אנא נסה שנית");
                Txt_Verification.Text = "";
                Txt_Verification.Focus();
            }
        }

        private void Txt_Verification_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void Send_VerifSMS()
        {
            string Mail_Address = "";
            string SMSBody = "";

            Mail_Address = GetPhoneNum();
            Random r = new Random();
            VerCode = r.Next(100000, 999999);

            SMSBody = $@"שלום, {Environment.NewLine} לשחרור מכבש {Mach.MacName}, הזן את הקוד הבא: {VerCode}";

            XmlWriter writer = XmlWriter.Create(@"\\172.16.1.20\\Drop\\AllianceSmsFile.xml");
            writer.WriteStartElement("root");
            writer.WriteStartElement("USER");
            writer.WriteElementString("USER_ID", "elians1");
            writer.WriteElementString("USER_PASS", "ku28pr");
            writer.WriteElementString("SENDER_NAME", "פקס");
            writer.WriteElementString("SENDER_ADDRESS", "@alliance.co.il");
            writer.WriteEndElement();
            writer.WriteStartElement("MESSAGES");
            writer.WriteStartElement("SMS_MESSAGE");
            writer.WriteElementString("EC_EVENT_ID", "0");
            writer.WriteElementString("EC_EVENT_NAME", "SMS");
            writer.WriteElementString("EC_MSG_ID", "0");
            writer.WriteElementString("TEXT", SMSBody);
            writer.WriteElementString("RECEPIENTS", Mail_Address);
            writer.WriteElementString("TIME", "0");
            writer.WriteElementString("TIMEOUT_TIME", "180");
            writer.WriteElementString("SIGNATURE", "046240624");
            writer.WriteEndElement();
            writer.Close();

            writer.Dispose();
        }

        private void Txt_Verification_KeyDown(object sender, KeyEventArgs e)
        {
            if (Txt_Verification.Text.Length == 6)
            {
                Btn_OK.Focus();
            }
        }

        public string GetPhoneNum()
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            string StrSql = $@"SELECT telefon
                               FROM isufkv.isav
                               WHERE oved='{"00" + Login.LoginForm.ID}'";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                return DT.Rows[0]["TELEFON"].ToString();
            }
            return "";
        }
    }
}
