﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace HMI_Curing
{
    public partial class Login : Form
    {
        bool test = false;
        public string ID;
        public static Login LoginForm;
        private MachineInfo mach;
        public string macName;
        DBService dbs = new DBService();
        public static int verCode = 0;
        
        public Login()
        {
            InitializeComponent();
        }

        public Login(MachineInfo m)
        {
            LoginForm = this;
            mach = m;
            InitializeComponent();
            if (Form1.loggedIn)
            {
                ShowReportScreen();
            }
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            DataTable DT = new DataTable();
            string StrSql = $@"select name1||name2||name3||name4||name5||name6||'.'||fmly10 
                            from hsali.hov left join isufkv.isav on hoovd =oved 
                            Where  hoovd = '00{UserTXT.Text}' AND hopsw='{PassTXT.Text}' ";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                Form1.WorkerName = DT.Rows[0][0].ToString();
                Form1.name = UserTXT.Text;
                ID = UserTXT.Text.Trim();
                ShowReportScreen();
                Form1.loggedIn = true; // דגל מנהל מחובר - כדי לאפשר למסך הדיווחים להיות פעיל כדקה ללא התחברות מחדש
            }
            else
            {
                MessageBox.Show("שם המשתמש או הסיסמא אינם נכונים. אנא נסה שוב");
            }
        }

        // מתודה להצגת התקלות לדיווח
        public void ShowReportScreen()
        {
            string StrSql = "";
            var index = Array.FindIndex(Form1.Arr_Lbl, row1 => row1.Mach.Trim() == mach.Mach);
            if (Form1.machineList[index].ActionCode == 1) // אם המכונה המדוברת בגיפור - נאפשר רק דיווח פתיחת פאנל
            {
                StrSql = $@"SELECT substring(IPROD,8,2)||' - '||trim(IDESC) as PR, substring(IPROD,8,2) as KOD
                            FROM BPCSFV30.IIML01
                            WHERE IPROD ='TAK-16199'";
            }
            else
            {
                StrSql = $@"SELECT substring(IPROD,8,2)||' - '||trim(IDESC) as PR, substring(IPROD,8,2) as KOD 
                        FROM BPCSFV30.IIML01
                        WHERE IPROD in('TAK-16101','TAK-16102','TAK-16103','TAK-16104','TAK-16105','TAK-16107','TAK-16108','TAK-16113','TAK-16116','TAK-16118','TAK-16119','TAK-16130','TAK-16142','TAK-16150','TAK-16112','TAK-16175','TAK-16180','TAK-16169','TAK-16199' ";
                StrSql += $@")";
                //// אם קובי מחובר - לתת לו אפשרות לשחרר מכבש
                //if (ID == "19531")
                //{
                //    StrSql += $@",'TAK-16198') ";
                //}
                //else
                //{
                //    StrSql += $@")";
                //}
            }
            listBox1.ValueMember = "KOD";
            listBox1.DisplayMember = "PR";
            DataTable D = dbs.executeSelectQueryNoParam(StrSql);
            listBox1.DataSource = D;
            LBL_LoggedIn.Text = "מנהל מחובר: " + Form1.WorkerName;

            label1.Visible = false;
            label2.Visible = false;
            UserTXT.Visible = false;
            PassTXT.Visible = false;
            LBL_LoggedIn.Visible = true;
            OKbtn.Visible = true;
            listBox1.Visible = true;
        }

        private void OKbtn_Click(object sender, EventArgs e)
        {
            string Kod = listBox1.SelectedValue.ToString();
            // אם התקלה היא חוסר צמיגים, נוודא שבאמת יש תכנון לצמיג ואז נשלח הודעה מתאימה
            if (Kod == "01")
            {
                DataTable DT = new DataTable();
                Press p = new Press(macName);
                var index = Array.FindIndex(Form1.Arr_Lbl, row1 => row1.Mach.Trim() == p.MacName.Trim());
                string currentTire = p.CurrentCuringTire();
                currentTire = p.GetGreenTireFromFinal(currentTire);
                string StrSql = $@"Call bpcsoali.in@372C('{currentTire.PadRight(15, ' ')}') ";
                dbs.executeInsertQuery(StrSql);
                StrSql = $@"SELECT ZPROD,ZQTY
                            FROM bpcsfali.in@372F ";
                DT = dbs.executeSelectQueryNoParam(StrSql);
                if (DT.Rows[0][0].ToString() == currentTire.Trim())
                {
                    if (decimal.Parse(DT.Rows[0][1].ToString()) > 0)
                    {
                        /////// במידה ונמצאה חוסר התאמה בין המלאי ברצפת הייצור למלאי במערכת, המערכת תאפשר למנהל המדווח לדווח על "שגיאת מלאי"
                        /////// בדיווח זה, ימלא המדווח את המלאי ברצפה והדיווח יעבור למחלקת תפ"י לתיקון המלאי
                        if ((MessageBox.Show(" לא ניתן לדווח חוסר צמיגים - לצמיג ישנו מלאי.  האם תרצה לדווח על מלאי שגוי? ", "שגיאת מלאי  ",
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                  MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes))
                        {
                            InventoryInput managerInput = new InventoryInput();
                            managerInput.StartPosition = FormStartPosition.CenterParent;
                            // Show testDialog as a modal dialog and determine if DialogResult = OK.
                            if (managerInput.ShowDialog(this) == DialogResult.OK)
                            {
                                string inventoryByManager = managerInput.GetText();
                                // דיווח תקלת מלאי שגוי
                                mach.UpdateTbls("41", Form1.name, Form1.WorkerName);
                                Send_Mail_Inventory(currentTire, DT.Rows[0][1].ToString(), inventoryByManager, Form1.Arr_Lbl[index].TireSize);
                            }
                            managerInput.Dispose();
                        }
                    }
                    else
                    {
                        //דיווח התקלה
                        mach.UpdateTbls(Kod, Form1.name, Form1.WorkerName);
                        Send_SMS();
                        Close();
                    }
                }
            }
            // אם התקלה היא אחת התקלות הנ"ל, נשלח הודעה לגורמים הרלוונטיים
            else if (Kod == "16" || Kod == "18" || Kod == "19")
            {
                // דיווח התקלה
                mach.UpdateTbls(Kod, Form1.name, Form1.WorkerName);
                Send_SMS();
            }
            else if (Kod  == "04") // דיווח החלפת תבנית - נאפשר למנהל לבחור את התבנית שנכנסת
            {
                ChooseMold choose = new ChooseMold(Form1.machineList.Find(x => x.MacName == macName));
                choose.ShowDialog();
                choose.StartPosition = FormStartPosition.CenterParent;
                // דיווח התקלה
                if (choose.DialogResult == DialogResult.OK)
                {
                    mach.UpdateTbls(Kod, Form1.name, Form1.WorkerName);
                }
            }
            else if (Kod == "99") // דיווח שחרור פאנל
            {
                if ((MessageBox.Show($" האם אתה בטוח שאתה רוצה לשחרר את פאנל מכבש {macName}? ", "שחרור פאנל מכבש  ",
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                  MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes))
                {
                    UnlockPanel managerInput = new UnlockPanel();
                    managerInput.StartPosition = FormStartPosition.CenterParent;
                    // Show testDialog as a modal dialog and determine if DialogResult = OK.
                    if (managerInput.ShowDialog(this) == DialogResult.OK)
                    {
                        try
                        {
                            // דיווח שחרור פאנל מכבש
                            var index = Array.FindIndex(Form1.Arr_Lbl, row1 => row1.Mach.Trim() == mach.Mach);

                            Form1.machineList[index].Oved = Form1.name; // עדכון העובד שמחובר כרגע

                            WcfServiceLibrary1.IService1 S = new WcfServiceLibrary1.Service1();

                            Opc.Da.ItemValueResult[] B = S.UnlockPanel(Form1.machineList[index].MacName, 1);

                            if (B != null && B[0] != null && B[0].Quality == Opc.Da.Quality.Good)
                            {
                                Form1.machineList[index].UpdatePanelUnBlock(managerInput.GetText());
                            }
                            else
                            {
                                MessageBox.Show(".אין אפשרות לשחרר פאנל מכבש זה. אנא נסה שנית" );
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(".אין אפשרות לשחרר פאנל ממכבש זה");
                        }
                    }
                    managerInput.Dispose();
                }
            }
            else if(Kod == "98") // דיווח שחרור מכבש
            {
                if ((MessageBox.Show($" האם אתה בטוח שאתה רוצה לשחרר את מכבש {macName}? ", "שחרור מכבש  ",
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                                  MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes))
                {
                    var index = Array.FindIndex(Form1.Arr_Lbl, row1 => row1.Mach.Trim() == mach.Mach);
                    UnlockPress managerInput = new UnlockPress(Form1.machineList[index]);
                    managerInput.StartPosition = FormStartPosition.CenterParent;
                    // Show testDialog as a modal dialog and determine if DialogResult = OK.
                    if (managerInput.ShowDialog(this) == DialogResult.OK)
                    {
                        try
                        {
                            // דיווח שחרור מכבש

                            //Form1.machineList[index].Oved = Form1.name; // עדכון העובד שמחובר כרגע

                            //WcfServiceLibrary1.IService1 S = new WcfServiceLibrary1.Service1();

                            //Opc.Da.ItemValueResult[] B = S.UnlockPanel(Form1.machineList[index].MacName, 1);

                            //if (B != null && B[0] != null && B[0].Quality == Opc.Da.Quality.Good)
                            //{
                            //    Form1.machineList[index].UpdatePanelUnBlock(managerInput.GetText());
                            //}
                            //else
                            //{
                            //    MessageBox.Show(".אין אפשרות לשחרר פאנל מכבש זה. אנא נסה שנית");
                            //}
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(".אין אפשרות לשחרר מכבש זה");
                        }
                    }
                    managerInput.Dispose();
                }
            }
            else
            {
                // דיווח התקלה
                mach.UpdateTbls(Kod, Form1.name, Form1.WorkerName);
            }
            this.Close();
        }


        ///// <summary>
        ///// מעדכן את טבלת המכבשים הפנימית כאשר משחררים ונועלים פאנל מכבש
        ///// </summary>
        ///// <param name="stat"> 0 - נעילת פאנל - איפוס הזמן ל מינמום
        /////                     1 - שחרור פאנל - כתיבת זמן נוכחי</param>  
        //public void UpdatePanelModifiedTime(int stat)
        //{
        //    if (stat == 0)
        //    {
        //        Form1.machineList.Find(x => x.MacName == mach.Mach).PanelReleaseTime = DateTime.MinValue;
        //    }
        //    else if (stat == 1)
        //    {
        //        Form1.machineList.Find(x => x.MacName == mach.Mach).PanelReleaseTime = DateTime.Now;
        //    }
        //}

        private void Send_SMS()
        {
            if (!test)
            {
                string Mail_Address = "";
                string SMSBody = "";

                // שליפת רשימת תפוצה לקבלת הודעה
                string connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=T:\Application\C#\Mailing_Address\Mail.mdb";
                OleDbConnection conn = new OleDbConnection(connectionString);
                string sql = " ";

                sql = "SELECT Mobile FROM SMS where Subject='HMI_Curing' and  Remark = '" + listBox1.SelectedValue.ToString() + "'";

                OleDbCommand cmd = new OleDbCommand(sql, conn);
                conn.Open();
                OleDbDataReader reader;
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Mail_Address = reader.GetString(0).ToString().Trim();
                }
                reader.Close();
                conn.Close();

                // Mail_Address = Mail_Address.Substring(0, Mail_Address.Length - 1);

                DataRowView row = (DataRowView)listBox1.SelectedItem;
                SMSBody = "שלום, " + Environment.NewLine + "דווחה   " + row[0] + " במכבש " + mach.Mach + " במחלקת גיפור." + Environment.NewLine + " אנא טפלו בתקלה בהקדם. ";

                XmlWriter writer = XmlWriter.Create(@"\\172.16.1.20\\Drop\\AllianceSmsFile.xml");
                writer.WriteStartElement("root");
                writer.WriteStartElement("USER");
                writer.WriteElementString("USER_ID", "elians1");
                writer.WriteElementString("USER_PASS", "ku28pr");
                writer.WriteElementString("SENDER_NAME", "פקס");
                writer.WriteElementString("SENDER_ADDRESS", "@alliance.co.il");
                writer.WriteEndElement();
                writer.WriteStartElement("MESSAGES");
                writer.WriteStartElement("SMS_MESSAGE");
                writer.WriteElementString("EC_EVENT_ID", "0");
                writer.WriteElementString("EC_EVENT_NAME", "SMS");
                writer.WriteElementString("EC_MSG_ID", "0");
                writer.WriteElementString("TEXT", SMSBody);
                writer.WriteElementString("RECEPIENTS", Mail_Address);
                writer.WriteElementString("TIME", "0");
                writer.WriteElementString("TIMEOUT_TIME", "180");
                writer.WriteElementString("SIGNATURE", "046240624");
                writer.WriteEndElement();
                writer.Close();
            }
        }

        // מייל על שגיאות מלאי
        private void Send_Mail_Inventory(string tire, string inventory, string inventoryByManager, string size)
        {
            if (!test)
            {
                //string Body = "";
                string Sbjct = "דיווח מלאי שגוי ";

                // Body = "שלום, " + Environment.NewLine + "במכבש " + macName + " דווחה שגיאת מלאי. להלן הפרטים: " + Environment.NewLine ;
                string MyHdr = "<div dir='rtl'>" + "שלום" + ",";
                MyHdr += "<BR>";
                MyHdr += "<BR>";
                MyHdr += "במכבש " + macName + " " + "דווחה שגיאת מלאי. להלן הפרטים: ";
                MyHdr += "<BR>";
                MyHdr += "<BR>";

                MailMessage message = new MailMessage();

                string connectionString = "Provider=Microsoft.JET.OLEDB.4.0;data source=T:\\Application\\C#\\Mailing_Address\\Mail.mdb";
                OleDbConnection conn = new OleDbConnection(connectionString);
                string sql = "";

                //// To Query
                sql = "SELECT Email FROM Mail where Subject='HMI_Curing' and ucase(Dest)='TO'";
                OleDbCommand cmd = new OleDbCommand(sql, conn);
                conn.Open();
                OleDbDataReader reader;
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    message.To.Add(new MailAddress((reader.GetString(0).ToString().Trim())));
                }
                reader.Close();

                // From Query
                sql = "SELECT Email FROM Mail where Subject='HMI_Curing' and ucase(Dest)='FROM'";
                cmd = new OleDbCommand(sql, conn);
                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    message.From = new MailAddress(reader.GetString(0).ToString().Trim());
                }
                reader.Close();
                conn.Close();

                // גוף המייל
                string StrHtml = "<table border=1 dir='rtl' style=' text-align:center; width:90%'> <tr style='text-decoration:'>";
                StrHtml += "<td colspan=1>מק'ט </td>";
                StrHtml += "<td colspan=1>גודל צמיג  </td>";
                StrHtml += "<td colspan=1>מלאי במערכת  </td>";
                StrHtml += "<td colspan=1>מלאי ברצפה  </td>";
                StrHtml += "<td colspan=1>מדווח  </td></tr></strong>";

                StrHtml += "<tr><td>" + tire + "</td>";
                StrHtml += "<td>" + size + "</td>";
                StrHtml += "<td>" + inventory + "</td>";
                StrHtml += "<td>" + inventoryByManager + "</td>";
                StrHtml += "<td>" + Form1.WorkerName + "</td></tr>";

                message.Subject = Sbjct;
                message.Body = MyHdr + "\n\r\n\r" + StrHtml;
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(message.Body, null, "text/html");
                message.AlternateViews.Add(htmlView);
                message.Priority = MailPriority.High;

                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                SmtpClient client = new SmtpClient();
                client.Host = "almail";// ServerIP;
                client.Send(message);
            }
        }


    }
}
