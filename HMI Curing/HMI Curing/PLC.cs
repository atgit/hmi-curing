﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.NetworkInformation;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace HMI_Curing
{
    public class PLC
    {


        Opc.Da.Item[] items;
        Opc.URL url;
        Opc.Da.Server server;
        OpcCom.Factory fact = new OpcCom.Factory();
        Opc.Da.Subscription groupRead;
        Opc.Da.SubscriptionState groupState;
        public string opcserver;
        int monitortime = 0;
        public List<Press> PressRegisters = new List<Press>(); // רשימת כתובות הרגיסטרים בבקרים

        public bool Connected { get; set; }


        public void Connect()
        {
            try
            {
                opcserver = "ALROPC";
                url = new Opc.URL("opcda://" + opcserver + "/RSLinx OPC Server");   // 1st: Create a server object and connect to the RSLinx OPC Server                                                                     
                server = new Opc.Da.Server(fact, null);
                server.Connect(url, new Opc.ConnectData(new System.Net.NetworkCredential()));   // 2nd: Connect to the created server  

                groupState = new Opc.Da.SubscriptionState();    // 3rd: Create a group if items            
                groupState.Name = "Group";
                groupState.UpdateRate = monitortime;   // this is the time between every reads from OPC server
                groupState.Active = true;   //t his must be true if you the group has to read value
                groupRead = (Opc.Da.Subscription)server.CreateSubscription(groupState);
                Connected = true;
            }
            catch (Exception ex)
            {
                Connected = false;
                Press P = new Press();
                P.WriteToLog("Error logging in to SERVER -- " + ex.Message);
            } 
        }

        public void Disconnect()
        {
            if (server.IsConnected)
            {
                server.Disconnect();
            }
        }

        public Opc.Da.ItemValueResult[] Read(Press P)
        {
            Opc.Da.ItemValueResult[] values = null;
            // מציאת נתוני רגיסטרי הבקר המתאים מרשימת הרגיסטרים
            var x = Form1.PressRegisters.First(z => z.MacName == P.MacName);

            if (x.IP != null)
            {
                if (PingHost(x.IP))
                {
                    items = new Opc.Da.Item[4];
                    values = new Opc.Da.ItemValueResult[items.Length];
                    items[0] = new Opc.Da.Item();
                    items[0].ItemName = "[Press_" + x.MacName + "]" + x.CuringBit.Trim() + ",L1,C1";
                    items[1] = new Opc.Da.Item();
                    items[1].ItemName = "[Press_" + x.MacName + "]" + x.HeatingBit.Trim() + ",L1,C1";
                    items[2] = new Opc.Da.Item();
                    items[2].ItemName = "[Press_" + x.MacName + "]" + x.Item.Trim() + ",L1,C1";
                    items[3] = new Opc.Da.Item();
                    items[3].ItemName = "[Press_" + x.MacName + "]" + x.CuringCycleBit.Trim() + ",L1,C1";

                    // Group Read
                    try
                    {
                        items = groupRead.AddItems(items);
                        values = groupRead.Read(items);
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            return values;
        }

        public void GetPLCAddresses(string mach)
        {
            DbServiceSQL dbsql = new DbServiceSQL();
            string StrSql = $@"SELECT * 
                               FROM PLC_Registers 
                               WHERE Press= ?";
            SqlParameter press = new SqlParameter("@press", mach);
            DataTable DT = dbsql.GetDataSetByQuery(StrSql, CommandType.Text, press).Tables[0];
            foreach (DataRow row in DT.Rows)
            {
                Press P = new Press(row["Press"].ToString());
                P.CuringBit = !string.IsNullOrEmpty(row["Curing_Bit"] as string) ? row["Curing_Bit"].ToString() : null;
                P.HeatingBit = !string.IsNullOrEmpty(row["Heating_Bit"] as string) ? row["Heating_Bit"].ToString() : null;
                P.InnerPressureBit = !string.IsNullOrEmpty(row["InnerPressure_Bit"] as string) ? row["InnerPressure_Bit"].ToString() : null;
                P.Item = !string.IsNullOrEmpty(row["Cat_Number"] as string) ? row["Cat_Number"].ToString() : null;
                P.IP = !string.IsNullOrEmpty(row["IP"] as string) ? row["IP"].ToString() : null;
                P.CuringCycleBit = !string.IsNullOrEmpty(row["Curing_Cycle"] as string) ? row["Curing_Cycle"].ToString() : null;
                P.PanelLockBit = !string.IsNullOrEmpty(row["Panel_Lock"] as string) ? row["Panel_Lock"].ToString() : null;
                PressRegisters.Add(P);
            }
        }

        public Opc.Da.ItemValueResult[] ReadDual307_308(Press P)
        {
            Opc.Da.ItemValueResult[] values = null;
            // מציאת נתוני רגיסטרי הבקר המתאים מרשימת הרגיסטרים
            var x = Form1.PressRegisters.First(z => z.MacName == P.MacName);

            if (x.IP != null)
            {
                if (PingHost(x.IP))
                {
                    string folder = "Press_307_308";

                    items = new Opc.Da.Item[5];
                    values = new Opc.Da.ItemValueResult[items.Length];
                    items[0] = new Opc.Da.Item();
                    items[0].ItemName = $@"[{folder}]" + x.CuringBit.Trim() + ",L1,C1";
                    items[1] = new Opc.Da.Item();
                    items[1].ItemName = $@"[{folder}]" + x.HeatingBit.Trim() + ",L1,C1";
                    items[2] = new Opc.Da.Item();
                    items[2].ItemName = $@"[{folder}]" + x.Item.Trim() + ",L1,C1";
                    items[3] = new Opc.Da.Item();
                    items[3].ItemName = $@"[{folder}]" + x.CuringCycleBit.Trim() + ",L1,C1";
                    items[4] = new Opc.Da.Item();
                    items[4].ItemName = $@"[{folder}]" + x.InnerPressureBit.Trim() + ",L1,C1";

                    // Group Read
                    try
                    {
                        items = groupRead.AddItems(items);
                        values = groupRead.Read(items);
                    }
                    catch (Exception e)
                    {

                    }
                }
            }
            return values;
        }

        public Opc.Da.ItemValueResult[] ReadDual(Press P)
        {
            Opc.Da.ItemValueResult[] values = null;
            // מציאת נתוני רגיסטרי הבקר המתאים מרשימת הרגיסטרים
            var x = Form1.PressRegisters.First(z => z.MacName == P.MacName);

            if (x.IP != null)
            {
                if (PingHost(x.IP))
                {
                    string folder = "";
                    if (x.MacName == "301" || x.MacName == "302")
                    {
                        folder = "Press_301_302";
                    }
                    else if (x.MacName == "303" || x.MacName == "304")
                    {
                        folder = "Press_303_304";
                    }
                    else if (x.MacName == "307" || x.MacName == "308")
                    {
                        folder = "Press_307_308";
                    }
                    else if (x.MacName == "311" || x.MacName == "312")
                    {
                        folder = "Press_311_312";
                    }
                    else if (x.MacName == "322" || x.MacName == "323")
                    {
                        folder = "Press_322_323";
                    }
                    else if (x.MacName == "313" || x.MacName == "314")
                    {
                        folder = "Press_313_314";
                    }

                    items = new Opc.Da.Item[4];
                    values = new Opc.Da.ItemValueResult[items.Length];
                    items[0] = new Opc.Da.Item();
                    items[0].ItemName = $@"[{folder}]" + x.InnerPressureBit.Trim() + ",L1,C1";
                    items[1] = new Opc.Da.Item();
                    items[1].ItemName = $@"[{folder}]" + x.HeatingBit.Trim() + ",L1,C1";
                    items[2] = new Opc.Da.Item();
                    items[2].ItemName = $@"[{folder}]" + x.Item.Trim() + ",L1,C1";
                    items[3] = new Opc.Da.Item();
                    items[3].ItemName = $@"[{folder}]" + x.CuringCycleBit.Trim() + ",L1,C1";

                    // Group Read
                    try
                    {
                        items = groupRead.AddItems(items);
                        values = groupRead.Read(items);
                    }
                    catch (Exception e)
                    {

                    }
                }
            }
            return values;
        }

        public void GetPLCAddresses()
        {
            DbServiceSQL dbsql = new DbServiceSQL();
            string StrSql = $@"SELECT * 
                               FROM PLC_Registers ";
            DataTable DT = dbsql.GetDataSetByQuery(StrSql, CommandType.Text).Tables[0];
            foreach (DataRow row in DT.Rows)
            {
                Press P = new Press(row["Press"].ToString());
                P.CuringBit = !string.IsNullOrEmpty(row["Curing_Bit"] as string) ? row["Curing_Bit"].ToString() : null;
                P.HeatingBit = !string.IsNullOrEmpty(row["Heating_Bit"] as string) ? row["Heating_Bit"].ToString() : null;
                P.InnerPressureBit = !string.IsNullOrEmpty(row["InnerPressure_Bit"] as string) ? row["InnerPressure_Bit"].ToString() : null;
                P.Item = !string.IsNullOrEmpty(row["Cat_Number"] as string) ? row["Cat_Number"].ToString() : null;
                P.IP = !string.IsNullOrEmpty(row["IP"] as string) ? row["IP"].ToString() : null;
                P.CuringCycleBit = !string.IsNullOrEmpty(row["Curing_Cycle"] as string) ? row["Curing_Cycle"].ToString() : null;
                P.PanelLockBit = !string.IsNullOrEmpty(row["Panel_Lock"] as string) ? row["Panel_Lock"].ToString() : null;
                PressRegisters.Add(P);
            }
        }





        public static bool PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }

        /// <summary>
        /// Function releases the PLC's panel for edit 
        /// </summary>
        /// <param name="P"> Press </param>
        /// <param name="action"></param>
        /// <returns></returns>
        public Opc.Da.ItemValueResult[] ReleasePanelForEdit(Press P,int action)
        {
            Opc.Da.Item[] items;
            Opc.URL url;
            Opc.Da.Server server;
            OpcCom.Factory fact = new OpcCom.Factory();
            Opc.Da.Subscription groupRead;
            Opc.Da.Subscription groupWrite;
            Opc.Da.SubscriptionState groupState;
            Opc.Da.SubscriptionState groupStateWrite;
            string opcserver;
            int monitortime = 0;

            opcserver = "ALBG";
            url = new Opc.URL("opcda://" + opcserver + "/RSLinx OPC Server");   // 1st: Create a server object and connect to the RSLinx OPC Server                                                                     
            server = new Opc.Da.Server(fact, null);
            server.Connect(url, new Opc.ConnectData(new System.Net.NetworkCredential()));   // 2nd: Connect to the created server            
            groupState = new Opc.Da.SubscriptionState();    // 3rd: Create a group if items            
            groupState.Name = "Group";
            groupState.UpdateRate = monitortime;   // this is the time between every reads from OPC server
            groupState.Active = true;   //t his must be true if you the group has to read value
            groupRead = (Opc.Da.Subscription)server.CreateSubscription(groupState);

            // Create a write group
            groupStateWrite = new Opc.Da.SubscriptionState();
            groupStateWrite.Name = "Group Write";
            groupStateWrite.Active = false;//not needed to read if you want to write only
            groupWrite = (Opc.Da.Subscription)server.CreateSubscription(groupStateWrite);


            var x = Form1.PressRegisters.First(z => z.MacName == P.MacName);

            items = new Opc.Da.Item[1];
            items[0] = new Opc.Da.Item();
            items[0].ItemName = $"[Press_{x.MacName}]{x.PanelLockBit},L1,C1";
            items = groupWrite.AddItems(items);

            //create the item that contains the value to write
            Opc.Da.ItemValue[] writeValues = new Opc.Da.ItemValue[1];
            writeValues[0] = new Opc.Da.ItemValue(items[0]);
            writeValues[0].Value = action;
            groupWrite.Write(writeValues);


            // Group Read
            Opc.Da.ItemValueResult[] values = null;
            items = new Opc.Da.Item[1];
            values = new Opc.Da.ItemValueResult[items.Length];
            items[0] = new Opc.Da.Item();
            items[0].ItemName = $"[Press_{x.MacName}]{x.PanelLockBit},L1,C1";

            items = groupRead.AddItems(items);
            values = groupRead.Read(items);

            server.Disconnect();
            return values;
        }

        /// <summary>
        /// Function Locks or Releases Press 
        /// Locks - When press is open
        /// Release - When successfully passed all inspections and a "41" record was recorded in LABELGP
        /// </summary>
        /// <param name="P"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public Opc.Da.ItemValueResult[] Press_Stop(Press P, int action)
        {
            Opc.Da.Item[] items;
            Opc.URL url;
            Opc.Da.Server server;
            OpcCom.Factory fact = new OpcCom.Factory();
            Opc.Da.Subscription groupRead;
            Opc.Da.Subscription groupWrite;
            Opc.Da.SubscriptionState groupState;
            Opc.Da.SubscriptionState groupStateWrite;
            string opcserver;
            int monitortime = 0;

            opcserver = "ALBG";
            url = new Opc.URL("opcda://" + opcserver + "/RSLinx OPC Server");   // 1st: Create a server object and connect to the RSLinx OPC Server                                                                     
            server = new Opc.Da.Server(fact, null);
            server.Connect(url, new Opc.ConnectData(new System.Net.NetworkCredential()));   // 2nd: Connect to the created server            
            groupState = new Opc.Da.SubscriptionState();    // 3rd: Create a group if items            
            groupState.Name = "Group";
            groupState.UpdateRate = monitortime;   // this is the time between every reads from OPC server
            groupState.Active = true;   //t his must be true if you the group has to read value
            groupRead = (Opc.Da.Subscription)server.CreateSubscription(groupState);

            // Create a write group
            groupStateWrite = new Opc.Da.SubscriptionState();
            groupStateWrite.Name = "Group Write";
            groupStateWrite.Active = false;//not needed to read if you want to write only
            groupWrite = (Opc.Da.Subscription)server.CreateSubscription(groupStateWrite);

            items = new Opc.Da.Item[1];
            items[0] = new Opc.Da.Item();
            items[0].ItemName = $"[Press_{P.MacName}]{P.PressLockBit},L1,C1";
            items = groupWrite.AddItems(items);

            //create the item that contains the value to write
            Opc.Da.ItemValue[] writeValues = new Opc.Da.ItemValue[1];
            writeValues[0] = new Opc.Da.ItemValue(items[0]);
            writeValues[0].Value = action;
            //groupWrite.Write(writeValues);

            // Group Read
            Opc.Da.ItemValueResult[] values = null;
            items = new Opc.Da.Item[1];
            values = new Opc.Da.ItemValueResult[items.Length];
            items[0] = new Opc.Da.Item();
            items[0].ItemName = $"[Press_{P.MacName}]{P.PressLockBit},L1,C1";

            items = groupRead.AddItems(items);
            values = groupRead.Read(items);

            server.Disconnect();
            return values;
        }
    }
}
