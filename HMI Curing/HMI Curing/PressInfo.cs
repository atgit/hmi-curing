﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HMI_Curing
{
    public partial class PressInfo : UserControl
    {
        decimal actualPlan;

        public decimal ActualPlan { get => actualPlan; set => actualPlan = value; }

        public PressInfo()
        {
            InitializeComponent();
        }

        public void TexlAlignment_Center()
        {
            Lbl_Press.TextAlign = ContentAlignment.MiddleCenter;
        }

        public void TexlAlignment_Left()
        {   Lbl_Press.TextAlign = ContentAlignment.MiddleLeft;}

        public void BackColor_Default()
        {
            Lbl_Press.BackColor = Color.WhiteSmoke;
            Lbl_NGT.BackColor = Color.WhiteSmoke;
            Lbl_BD.BackColor = Color.WhiteSmoke;
            Lbl_QA.BackColor = Color.WhiteSmoke;
            Lbl_Setups.BackColor = Color.WhiteSmoke;
            Lbl_Molds.BackColor = Color.WhiteSmoke;            
        }

        public void SetPressNo(string PressNo)
        {   Lbl_Press.Text = PressNo;}

        public string GetPressNo()
        {   return Lbl_Press.Text;}

        public void Resize_Lbl_Press(int W,int H)
        { Lbl_Press.Size = new Size(W, H); }

        public void SetPressPerformance(decimal OPLAN, decimal OMADE)
        {
            if (OPLAN != 0)
            {
                Lbl_Press.Text = (OPLAN + " / " + OMADE).PadRight(12, ' ') + Math.Round((OMADE / OPLAN * 100), 0) + "%";
                if (OMADE / OPLAN * 100 > 85)
                    SetBackcolor(Color.Green);
                else if (OMADE / OPLAN * 100 > 70)
                    SetBackcolor(Color.Yellow);
                else
                    SetBackcolor(Color.IndianRed);
            }
            else
            {
                Lbl_Press.Text = "0 / 0 ".PadRight(12, ' ') + "0%";
                SetBackcolor(Color.LightGray);
            }               
        }

        public void StopLack_BackColor(int Index, Color C)
        {
            if (Index == 1)
                Set_NGT_Backcolor(C);
            else if (Index == 2)
                Set_BD_Backcolor(C);
            else if (Index == 3)
                Set_QA_Backcolor(C);
            else if (Index == 4)
                Set_Setups_Backcolor(C);
            else if (Index == 5)
                Set_Molds_Backcolor(C);
        }

        public void StopLackLast3_BackColor(int Index, Color C)
        {
            if (Index == 1)
                Set_NGT_Backcolor(C);
            else if (Index == 2)
            {
                if (Lbl_NGT.BackColor != C)
                {
                    Set_BD_Backcolor(C);
                }
            }
            else if (Index == 3)
            {
                if (Lbl_NGT.BackColor != C && Lbl_BD.BackColor != C)
                {
                    Set_QA_Backcolor(C);
                }
            }
            else if (Index == 4)
            {
                if (Lbl_NGT.BackColor != C && Lbl_BD.BackColor != C && Lbl_QA.BackColor != C)
                {
                    Set_Setups_Backcolor(C);
                }
            }
            else if (Index == 5)
            {
                if (Lbl_NGT.BackColor != C && Lbl_BD.BackColor != C && Lbl_QA.BackColor != C && Lbl_Setups.BackColor != C)
                {
                    Set_Molds_Backcolor(C);
                }
            }
        }

        //public void StopLackLast3_BackColor(int Index, Color C)
        //{
        //    if (Index == 1)
        //        Set_NGT_Backcolor(C);
        //    else if (Index == 2)
        //        Set_BD_Backcolor(C);
        //    else if (Index == 3)
        //        Set_QA_Backcolor(C);
        //    else if (Index == 4)
        //        Set_Setups_Backcolor(C);
        //    else if (Index == 5)
        //        Set_Molds_Backcolor(C);
        //}

        public void SetBackcolor(Color C)
        {
            Set_Press_Backcolor(C);
            Set_NGT_Backcolor(C);
            Set_BD_Backcolor(C);
            Set_QA_Backcolor(C);
            Set_Setups_Backcolor(C);
            Set_Molds_Backcolor(C);
        }        
        
        public void Set_Press_Backcolor(Color C)
        {   Lbl_Press.BackColor = C;}

        public void Set_NGT_Backcolor(Color C)
        { Lbl_NGT.BackColor = C; }

        public void Set_BD_Backcolor(Color C)
        { Lbl_BD.BackColor = C; }

        public void Set_QA_Backcolor(Color C)
        { Lbl_QA.BackColor = C; }

        public void Set_Setups_Backcolor(Color C)
        { Lbl_Setups.BackColor = C; }

        public void Set_Molds_Backcolor(Color C)
        { Lbl_Molds.BackColor = C; }

        public void ResizeLBL(int Size)
        {
            if (Size == 1)
            {
                Height = Convert.ToInt32(Height * 0.9);
                Width = Convert.ToInt32(Width * 0.9);
                //NameLBL.Height = 19;
                //SizeLBL.Height = 19;
                //TimeLBL.Height = 19;
                //NameLBL.Font = new Font("Tahoma", 11, FontStyle.Bold);
                //SizeLBL.Font = new Font("Tahoma", 11, FontStyle.Bold);
                //TimeLBL.Font = new Font("Tahoma", 11, FontStyle.Bold);
            }
            else if (Size == 2)
            {
                this.Height = 24;
                this.Width = 100;
                //NameLBL.Height = 13;
                //SizeLBL.Height = 13;
                //TimeLBL.Height = 13;
                //NameLBL.Font = new Font("Tahoma", 9, FontStyle.Bold);
                //SizeLBL.Font = new Font("Tahoma", 9, FontStyle.Bold);
                //TimeLBL.Font = new Font("Tahoma", 9, FontStyle.Bold);
            }
        }
    }
}
