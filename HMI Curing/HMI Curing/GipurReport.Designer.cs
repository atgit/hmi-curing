﻿namespace HMI_Curing
{
    partial class GipurReport
    {
        /// <summary>
        /// RequiIndianRed designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// RequiIndianRed method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Btn_Submit = new System.Windows.Forms.Button();
            this.Btn_Split = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(36, 230);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dataGridView1.Size = new System.Drawing.Size(1170, 318);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick_1);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            // 
            // Btn_Submit
            // 
            this.Btn_Submit.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Btn_Submit.Location = new System.Drawing.Point(547, 568);
            this.Btn_Submit.Name = "Btn_Submit";
            this.Btn_Submit.Size = new System.Drawing.Size(173, 31);
            this.Btn_Submit.TabIndex = 1;
            this.Btn_Submit.Text = "עדכן";
            this.Btn_Submit.UseVisualStyleBackColor = true;
            this.Btn_Submit.Click += new System.EventHandler(this.Btn_Submit_Click);
            // 
            // Btn_Split
            // 
            this.Btn_Split.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Btn_Split.Location = new System.Drawing.Point(1033, 193);
            this.Btn_Split.Name = "Btn_Split";
            this.Btn_Split.Size = new System.Drawing.Size(173, 31);
            this.Btn_Split.TabIndex = 2;
            this.Btn_Split.Text = "פצל שורה מסומנת";
            this.Btn_Split.UseVisualStyleBackColor = true;
            this.Btn_Split.Click += new System.EventHandler(this.Btn_Split_Click);
            // 
            // GipurReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::HMI_Curing.Properties.Resources.ATG_Wallpaper_1440x900;
            this.ClientSize = new System.Drawing.Size(1230, 647);
            this.Controls.Add(this.Btn_Split);
            this.Controls.Add(this.Btn_Submit);
            this.Controls.Add(this.dataGridView1);
            this.Name = "GipurReport";
            this.Text = "GipurReport";
            this.Load += new System.EventHandler(this.GipurReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button Btn_Submit;
        private System.Windows.Forms.Button Btn_Split;
    }
}