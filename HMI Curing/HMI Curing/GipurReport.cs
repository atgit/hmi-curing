﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HMI_Curing
{
    public partial class GipurReport : Form
    {
        DataTable DT = new DataTable();
        DateTimePicker dtp = new DateTimePicker();
        Rectangle _Rectangle;
        string lib = "pmic";
        OleDbCommand command;
        OleDbDataAdapter da;
        private BindingSource bindingSource = null;
        private OleDbCommandBuilder oleCommandBuilder = null;
        DataTable dataTable = new DataTable();
        String connectionString = "Provider = IBMDA400; Data Source = 172.16.1.158; User ID = as400; Password = as400";//use your connection string please

        public GipurReport()
        {
            InitializeComponent();
            dataGridView1.Controls.Add(dtp);
            dtp.Visible = false;
            dtp.Format = DateTimePickerFormat.Time;
            dtp.CustomFormat = "hh:mm";
            dtp.ShowUpDown = true;
            dtp.TextChanged += new EventHandler(dtp_TextChange);
            dtp.Leave += new EventHandler(dtp_Leave);
        }

        private void DataBind()
        {
            dataGridView1.DataSource = null;
            dataTable.Clear();

            String queryString1 = @"select * from " + lib + ".taxilogp " +
                              "where LTIMESTAMP between '2017-07-13-07.42.39.000000'  and '2017-07-17-10.24.28.000000'"; // Use your table please

            OleDbConnection connection = new OleDbConnection(connectionString);
            connection.Open();
            OleDbCommand command = connection.CreateCommand();
            command.CommandText = queryString1;
            try
            {
                da = new OleDbDataAdapter(queryString1, connection);
                oleCommandBuilder = new OleDbCommandBuilder(da);
                da.Fill(dataTable);
                DataColumn[] D = new DataColumn[1];
                D[0] = dataTable.Columns["LTIMESTAMP"];
                dataTable.PrimaryKey = D;
                bindingSource = new BindingSource { DataSource = dataTable };
                dataGridView1.DataSource = bindingSource;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void GipurReport_Load(object sender, EventArgs e)
        {
            //LoadDGVData();
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");
            DataBind();
        }

        public void LoadDGVData()
        {
            //dataGridView1.Columns["LTIMESTAMP"].DefaultCellStyle.Format = "HH:mm:ss"; // פורמט השעה
            DBService dbs = new DBService();
            string StrSql = @"select * from pmic.taxilogp
                              where LTIMESTAMP between '2017-07-26-08.42.39.000000'  and '2017-07-26-10.24.28.000000' ";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            dataGridView1.DataSource = DT;
            //dataGridView1.Columns["LTIMESTAMP"]
            //dataGridView1.Columns["LTIMESTAMP"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        }

        private void Btn_Split_Click(object sender, EventArgs e)
        {
            int t = dataGridView1.CurrentCell.RowIndex;
            DataTable DT1 = new DataTable();
            DT1 = DT;
            dataGridView1.DataSource = DT1;
            DataRow R = DT.NewRow();
            DT1.Rows.InsertAt(R, t);
            DT = DT1;
        }


        private void dtp_TextChange(object sender, EventArgs e)
        {
            dataGridView1.CurrentCell.Value = dtp.Text.ToString();
        }

        // עדכון התצוגה משעה לתאריך ושעה שיהיה קריא יותר
        private void dtp_Leave(object sender, EventArgs e)
        {
            dataGridView1.CurrentCell.Value = dtp.Value.ToString("yyyy-MM-dd.HH.mm.ss.sss");
        }

        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            dtp.Visible = false;

        }


        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            dtp.Visible = false;
        }

        private void dataGridView1_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            switch (dataGridView1.Columns[e.ColumnIndex].Name)
            {
                case "LTIMESTAMP":

                    _Rectangle = dataGridView1.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true); //  
                    dtp.Size = new Size(_Rectangle.Width, _Rectangle.Height); //  
                    dtp.Location = new Point(_Rectangle.X, _Rectangle.Y); //  
                    dtp.Visible = true;
                    
                    break;
            }
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
        //    if (e.ColumnIndex == dataGridView1.Columns["LTIMESTAMP"].Index)
        //    {
        //        DataGridViewCell cell = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex];
        //        if (cell != null)
        //        {
        //            cell.Value = DateTime.ParseExact(cell.Value.ToString(), "dd/MM/yyyy HH.mm.ss",
        //CultureInfo.InvariantCulture
        //);
        //        }
        //    }
        }

        private void Btn_Submit_Click(object sender, EventArgs e)
        {

            //dataGridView1.EndEdit(); //very important step
            //da.Update(dataTable);
            //MessageBox.Show("Updated");
            //DataBind();
            bindingSource.EndEdit();
            dataGridView1.EndEdit();
            DataTable dt = (DataTable)bindingSource.DataSource;

            // Just for test.... Try this with or without the EndEdit....
            DataTable changedTable = dt.GetChanges();
            Console.WriteLine(changedTable.Rows.Count);
            OleDbConnection Con =  new OleDbConnection("Provider = IBMDA400; Data Source = 172.16.1.158; User ID = as400; Password = as400");//use your connection string please

            //   OleDbCommand command = new OleDbCommand("UPDATE pmic.taxilogp set LACT=@act,LTAK=@tak WHERE LTIMESTAMP=@time ",Con);
            OleDbCommand command = new OleDbCommand("UPDATE pmic.taxilogp set LACT='4',LTAK='69' WHERE LTIMESTAMP>='2017-06-12-06.24.17.000000' ", Con);
            //command.Parameters.Add("@act", OleDbType.LongVarChar, 2, "2");
            //command.Parameters.Add("@tak", OleDbType.LongVarChar, 2, "69");
            //command.Parameters.Add("@time", OleDbType.LongVarChar, 26,"2017-06-12-06.24.17.000000");
            //command.Parameters.Add("@lib", OleDbType.LongVarChar, 3, lib);
            da.UpdateCommand = command;
            new OleDbCommandBuilder(da);

            dt.AcceptChanges();
            changedTable.AcceptChanges();
            int rowsUpdated = da.Update(dt);
        
            DataBind();
            Console.WriteLine(rowsUpdated);
        }
    }
}
