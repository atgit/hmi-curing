﻿namespace HMI_Curing
{
    partial class AwaitedTyres
    {
        /// <summary>
        /// RequiIndianRed designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// RequiIndianRed method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AwaitedTyres));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.MachLBL = new System.Windows.Forms.Label();
            this.PringBTN = new System.Windows.Forms.Button();
            this.OKBTN = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 37);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(1041, 278);
            this.dataGridView1.TabIndex = 0;
            // 
            // MachLBL
            // 
            this.MachLBL.AutoSize = true;
            this.MachLBL.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.MachLBL.Location = new System.Drawing.Point(12, 9);
            this.MachLBL.Name = "MachLBL";
            this.MachLBL.Size = new System.Drawing.Size(0, 24);
            this.MachLBL.TabIndex = 1;
            // 
            // PringBTN
            // 
            this.PringBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.PringBTN.Location = new System.Drawing.Point(404, 379);
            this.PringBTN.Name = "PringBTN";
            this.PringBTN.Size = new System.Drawing.Size(75, 23);
            this.PringBTN.TabIndex = 2;
            this.PringBTN.Text = "Print";
            this.PringBTN.UseVisualStyleBackColor = true;
            this.PringBTN.Click += new System.EventHandler(this.PringBTN_Click);
            // 
            // OKBTN
            // 
            this.OKBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.OKBTN.Location = new System.Drawing.Point(593, 379);
            this.OKBTN.Name = "OKBTN";
            this.OKBTN.Size = new System.Drawing.Size(75, 23);
            this.OKBTN.TabIndex = 3;
            this.OKBTN.Text = "O.K";
            this.OKBTN.UseVisualStyleBackColor = true;
            this.OKBTN.Click += new System.EventHandler(this.OKBTN_Click);
            // 
            // AwaitedTyres
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 436);
            this.Controls.Add(this.OKBTN);
            this.Controls.Add(this.PringBTN);
            this.Controls.Add(this.MachLBL);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AwaitedTyres";
            this.Text = "AwaitedTyres";
            this.Load += new System.EventHandler(this.AwaitedTyres_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label MachLBL;
        private System.Windows.Forms.Button PringBTN;
        private System.Windows.Forms.Button OKBTN;
    }
}