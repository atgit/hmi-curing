﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary;

namespace HMI_Curing
{
    public class Press : Machine
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //Opc.Da.Item[] items;
        //Opc.URL url;
        //Opc.Da.Server server;
        //OpcCom.Factory fact = new OpcCom.Factory();
        //Opc.Da.Subscription groupRead;
        //Opc.Da.SubscriptionState groupState;
        //string opcserver;

        int teken;
        string StrSql;
        string tapiali = "tapiali";
        string Lib1 = "rzpali";
        StringBuilder sb;

        public string Size { get; set; }
        public string Item { get; set; }
        public string Oved { get; set; }
        public string F_Name { get; set; }
        public string L_Name { get; set; }
        public string Status { get; set; }
        public int TakCode { get; set; }
        public int ActionCode { get; set; }
        public string Pos15 { get; set; }
        public int Column { get; set; }
        public bool FirstTire { get; set; }
        public string DNA { get; set; }
        public DateTime TimeStamp { get; set; }
        //// כתובות בבקרים
        public string InnerPressureBit { get; set; }
        public string HeatingBit { get; set; }
        public string CuringBit { get; set; }
        public string CuringCycleBit { get; set; }
        public string PanelLockBit { get; set; }
        public string PressLockBit { get; set; }
        public string IP { get; set; }
        // תוצאות של כתובות בבקר
        public bool IsCuring { get; set; }
        public bool IsHeating { get; set; }
        public bool ReadingError { get; set; }
        public DateTime PanelReleaseTime { get; set; }
        public int Teken
        {
            get
            {
                return teken;
            }
            set
            {

                teken = value;
            }
        }

        public Press() : base()
        {
            ReadingError = true;
        }

        public Press(string name)
        {
            ReadingError = true;
            MacName = name;
        }

        //PRSSTTP הפונקציה בודקת את סטטוס המכבש, האם צמיג ראשון או לא, בטבלה 
        public string checkStt()
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            StrSql = $@"SELECT MHSTT 
                        FROM tapiali.PRSSTTP 
                        WHERE MHMACH='{MacName}'";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            return DT.Rows[0]["MHSTT"].ToString();
        }

        // לבדוק כל פעם שמשתמשים בזה אם הגענו ל סטופ!!!
        public string CurrentCuringTire()
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            DataTable DT1 = new DataTable();
            int fromDate = int.Parse(DateTime.Now.AddDays(-1).ToString("yyMMdd"));
            int toDate = int.Parse(DateTime.Now.ToString("yyMMdd"));
            string DateNow = DateTime.Now.ToString("yyMMdd.HHmm");
            string StrSql = $@"Select izprod 
                              FROM BPCSFALI.shmal01
                              WHERE {DateNow} between IZFRYY*10000 +  IZFRMM*100 +  IZFRDD+cast(IZFRHR/100+IZFRMN/10000 as decimal(4,4))  
                              AND IZTOYY*10000 +  IZTOMM*100 +  IZTODD+cast(IZTOHR/100+IZTOMN/10000 as decimal(4,4)) and izmach='{MacName}' ";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                return DT.Rows[0][0].ToString();
            }
            else
            {
                return "STOP";
            }
        }

        public string GetGreenTireFromFinal(string final)
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            //final = final.PadRight(15, ' ');
            string StrSql = $@"SELECT BCHLD 
                               FROM BPCSFV30.mbml01 
                               WHERE bprod='{final}' and substring(bclac,1,1) in ('N' , 'M') and substring(bclac,2,1) between '1' and '9' ";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                return DT.Rows[0][0].ToString().PadRight(15, ' ');
            }
            else
            {
                return "STOP";
            }
        }

        public void ReadPLC()
        {
            Opc.Da.ItemValueResult[] values = Form1.Pl.Read(this);

            try
            {
                if (values != null && values[0] != null && values[0].Quality == Opc.Da.Quality.Good)
                {
                    // Curing_Bit
                    if (values[0].Value.ToString() == "1")
                    {
                        IsCuring = true;
                        //WriteToTestLog(1);
                    }
                    else
                    {
                        IsCuring = false;
                        //WriteToTestLog(0);
                    }
                    // Heating_Bit
                    if (values[1].Value.ToString() == "1" && !IsCuring)
                    {
                        IsHeating = true;
                    }
                    else
                    {
                        IsHeating = false;
                    }
                    // Cat_Number_Bit
                    if (!string.IsNullOrEmpty(values[2].Value as string) && !string.IsNullOrWhiteSpace(values[2].Value as string))
                    {
                        Item = values[2].Value.ToString().Trim(new char[] { '\\', '0' }).PadRight(15, ' ');
                    }
                    // CuringCycleBit --> Get The Technological Curing Time
                    if (!string.IsNullOrEmpty(values[3].Value as string) && !string.IsNullOrWhiteSpace(values[3].Value as string))
                    {
                        string x = values[3].Value.ToString().Split('\\').First();
                        int i = x.IndexOf('-');
                        i = x.IndexOf('-', i + 1);
                        Teken = int.Parse(x.Substring(i + 1));
                    }
                    ReadingError = false; // אין בעיה בקריאת הבקרים
                }
                else
                {
                    WriteToTestLog(3);
                    ReadingError = true; // ישנה בעיה בקריאת הבקרים
                }
            }
            catch (Exception ex)
            {
                WriteToLog(ex.Message);
            }
        }

        public void ReadPLCDual()
        {
            Opc.Da.ItemValueResult[] values = Form1.Pl.ReadDual(this);

            try
            {
                if (values != null && values[0] != null && values[0].Quality == Opc.Da.Quality.Good)
                {
                    // Curing_Bit
                    if (int.Parse(values[0].Value.ToString()) > 50)
                    {
                        IsCuring = true;
                        //WriteToTestLog(1);
                    }
                    else
                    {
                        IsCuring = false;
                        //WriteToTestLog(0);
                    }
                    // Heating_Bit
                    if (values[1].Value.ToString() == "1" && !IsCuring)
                    {
                        IsHeating = true;
                        //WriteToTestLog(2);
                    }
                    else
                    {
                        IsHeating = false;
                    }
                    // Cat_Number_Bit
                    if (!string.IsNullOrEmpty(values[2].Value as string))
                    {
                        Item = values[2].Value.ToString().Trim(new char[] { '\\', '0' }).PadRight(15, ' ');
                    }
                    // CuringCycleBit --> Get The Technological Curing Time
                    if (!string.IsNullOrEmpty(values[3].Value as string))
                    {
                        //string x = values[3].Value.ToString().Trim(new char[] { '\\', '0' });
                        string x = values[3].Value.ToString().Split('\\').First();
                        int i = x.IndexOf('-');
                        i = x.IndexOf('-', i + 1);
                        Teken = int.Parse(x.Substring(i + 1));
                        //// הוספת ספרת ה 0 האחרונה במידה וחתכנו אותה
                        //Teken = Teken <= 30 ? Teken * 10 : Teken;
                        //// שוב פעם אותה בדיקה בדיוק - כדי לתפוס את הפעמים שמחזור הגיפור הוא 100 ונשמטים שני אפסים
                        //Teken = Teken <= 30 ? Teken * 10 : Teken;
                    }
                    ReadingError = false; // אין בעיה בקריאת הבקרים
                }
                else
                {
                    WriteToTestLog(3);
                    ReadingError = true; // ישנה בעיה בקריאת הבקרים
                }
            }
            catch (Exception ex)
            {
                WriteToLog(ex.Message);
            }
        }

        public void ReadPLC_307_308()
        {
            Opc.Da.ItemValueResult[] values = Form1.Pl.ReadDual307_308(this);

            try
            {
                if (values != null && values[0] != null && values[0].Quality == Opc.Da.Quality.Good)
                {
                    // Curing_Bit
                    if (values[0].Value.ToString() == "1")
                    {
                        if (values[4].Value.ToString() == "1")
                        {
                            IsCuring = true;
                        }

                        //WriteToTestLog(1);
                    }
                    else
                    {
                        IsCuring = false;
                        //  WriteToTestLog(0);
                    }
                    // Heating_Bit
                    if (values[1].Value.ToString() == "1" && !IsCuring)
                    {
                        IsHeating = true;
                    }
                    else
                    {
                        IsHeating = false;
                    }
                    // Cat_Number_Bit
                    if (!string.IsNullOrEmpty(values[2].Value as string))
                    {
                        Item = values[2].Value.ToString().Trim(new char[] { '\\', '0' }).PadRight(15, ' ');
                    }
                    // CuringCycleBit --> Get The Technological Curing Time
                    if (!string.IsNullOrEmpty(values[3].Value as string))
                    {
                        //string x = values[3].Value.ToString().Trim(new char[] { '\\', '0' });
                        string x = values[3].Value.ToString().Split('\\').First();
                        int i = x.IndexOf('-');
                        i = x.IndexOf('-', i + 1);
                        Teken = int.Parse(x.Substring(i + 1));
                        //// הוספת ספרת ה 0 האחרונה במידה וחתכנו אותה
                        //Teken = Teken <= 30 ? Teken * 10 : Teken;
                        //// שוב פעם אותה בדיקה בדיוק - כדי לתפוס את הפעמים שמחזור הגיפור הוא 100 ונשמטים שני אפסים
                        //Teken = Teken <= 30 ? Teken * 10 : Teken;
                    }
                    ReadingError = false; // אין בעיה בקריאת הבקרים
                }
                else
                {
                    WriteToTestLog(3);
                    ReadingError = true; // ישנה בעיה בקריאת הבקרים
                }
            }
            catch (Exception ex)
            {
                WriteToLog(ex.Message);
            }
        }

        public void WriteToTestLog(int val)
        {
            DbServiceSQL dbs = new DbServiceSQL();
            StrSql = $@"INSERT INTO Test
                        VALUES('{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}','{MacName}',{val},'{Form1.Pl.opcserver}')";
            dbs.ExecuteQuery(StrSql);
        }


        //מתודה לכתיבת לוג מעקב אחרי האפליקציה.בינתיים תנטר לנו את סגירות המשמרת
        public void WriteToLog(string logData)
        {
            // לוג מערכת למעקב אחרי ריצות
            if (File.Exists(@"T:\Application\C#\HMI Curing\Log.txt"))
            {
                sb = new StringBuilder();
                sb.Append(logData + "  -  " + DateTime.Now);

                File.AppendAllText(@"T:\Application\C#\HMI Curing\" + "Log.txt", sb.ToString() + Environment.NewLine);
                sb.Clear();
            }
        }

        public void UpdateEndOfShift(DateTime current)
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();

            //Rzpali/taxisttp עדכון התקלה לזמן תחילת המשמרת בקובץ 
            StrSql = $@"UPDATE {Lib1}.TAXISTTP set stimestamp='{current.ToString("yyyy-MM-dd-HH.mm.ss.00000")}' 
                        WHERE spress='{MacName.Trim()}'";
            dbs.executeInsertQuery(StrSql);

            // שינוי מתאריך 16/03/2017
            // הוספת בדיקה אם התקלה קטנה מ-10, אם כן - נרפד לה 0 מוביל
            if (TakCode.ToString().Length < 2)
            {
                InsertEndShiftToLog(current);
            }
            else
            {
                InsertEndShiftToLog(current);
            }

        }


        public bool DidReadDNA()
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            StrSql = $@"SELECT LACTAN
                        FROM tapiali.labelgL52
                        WHERE LMACH='{MacName.Trim()}' and LACTAN in (41,42) AND LERRCD = '' and lacndt <= {DateTime.Now.ToString("yyyyMMdd")}
                        ORDER by lacndt desc,lacntm desc fetch first row only ";
            try
            {
                DT = dbs.executeSelectQueryNoParam(StrSql);
            }
            catch (Exception ex)
            {
                throw;
            }

            foreach (DataRow row in DT.Rows)
            {
                if (row["LACTAN"].ToString() == "41")
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// פונקציה לדיווח פריקה אוטומטית מהבקר
        /// </summary>
        public void ReportPressOpen()
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            // שליפת הנתונים מרשומת ה 41
            StrSql = $@"SELECT BPROD, LYYWW, LMACH, LACNDT, LACNTM, LACTAN, LDEPT, LWRKC, LLBLNA, LERRCD, MHSTT
                    FROM {tapiali}.labelgL52
                    Left join bpcsfv30.mbml01 on bchld = lprod
                    Left join rzpali.mcovip on bprod = oprit and lmach = omach and lshift = oshift and odate = lacndt - 19000000
                    Left join {tapiali}.prssttp on lmach=mhmach
                    WHERE LMACH='{MacName.Trim()}' and LACTAN in (41,42) 
                    ORDER by lacndt desc,lacntm desc fetch first row only";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                // אם הרשומה האחרונה היא טעינה - נזין רשומת פריקה. אם לא - נתעלם
                if (DT.Rows[0]["LACTAN"].ToString() == "41")
                {
                    // אם השדה ריק אזי הרשומה אינה פיקטיבית - נזין רשומת 42
                    if (DT.Rows[0]["LERRCD"].ToString() == "")
                    {
                        string LSTT = " ";
                        //LSTT ונוסיף 1 בשדה  PRSSTTP בדיקה האם המכבש בסטטוס 5 או 7 - במידה וכן נעדכן ל 6 ב 
                        if (DT.Rows[0]["MHSTT"].ToString() == "7" || DT.Rows[0]["MHSTT"].ToString() == "5")
                        {
                            UpdatePRSSTTP(6);
                            LSTT = "1";
                        }

                        StrSql = $@"INSERT INTO {tapiali}.labelgp  (LPROD,LYYWW,LMACH,LACNDT,LACNTM,LLOGNO,LACTAN,LDEPT,LWRKC,LSHIFT,LSTT,LLBLNA,LUSER,LWSID)
                        VALUES('{DT.Rows[0]["BPROD"].ToString().PadRight(15, ' ').Substring(0, 14)}',{DT.Rows[0]["LYYWW"]},'{DT.Rows[0]["LMACH"]}',{DateTime.Now.ToString("yyyyMMdd")},{DateTime.Now.ToString("HHmmss")},(
                                        select max(llogno)
                                        from {tapiali}.labelgp) +1,42,{DT.Rows[0]["LDEPT"]},{DT.Rows[0]["LWRKC"]},'{dbs.GetShiftNum()}','{LSTT}',{DT.Rows[0]["LLBLNA"]},'CSHARP','{Environment.MachineName.PadRight(10, ' ').Substring(0, 10)}'
                                     )";
                        dbs.executeInsertQuery(StrSql);
                    }
                }
            }
        }

        // עדכון הקובץ ל סטטוס 6 
        public void UpdatePRSSTTP(int val)
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            StrSql = $@"UPDATE {tapiali}.PRSSTTP
                        SET MHSTT = '{val}'
                        WHERE MHMACH = '{MacName}'";
            dbs.executeUpdateQuery(StrSql);
        }

        /// <summary>
        /// STTP עדכון קובץ 
        /// עם עדכון שדה התקן
        /// </summary>
        /// <param name="teken"></param>
        public void UpdateTAXISTTP(bool teken)
        {
            DBService dbs = new DBService();
            /// עדכון הטבלה במסד הנתונים
            StrSql = $@"UPDATE {Lib1}.TAXISTTP
                        SET stimestamp='{TimeStamp.ToString("yyyy-MM-dd-HH.mm.ss.00000")}',sact='{ActionCode}', semp='{Oved}', stak='{TakCode.ToString().PadLeft(2, '0')}',sprod='{Item.PadRight(15, ' ').Substring(0, 15)}', sstdtm='{Teken}' 
                        WHERE spress='{MacName}'";
            dbs.executeInsertQuery(StrSql);
        }
        /// <summary>
        /// STTP עדכון קובץ 
        /// </summary>
        public void UpdateTAXISTTP()
        {
            DBService dbs = new DBService();
            /// עדכון הטבלה במסד הנתונים
            StrSql = $@"UPDATE {Lib1}.TAXISTTP
                        SET stimestamp='{TimeStamp.ToString("yyyy-MM-dd-HH.mm.ss.00000")}',sact='{ActionCode}', semp='{Oved}', stak='{TakCode.ToString().PadLeft(2, '0')}',sprod='{Item.PadRight(15, ' ').Substring(0, 15)}'
                        WHERE spress='{MacName}'";
            dbs.executeInsertQuery(StrSql);
        }
        /// <summary>
        /// הוספת רשומה לקובץ הלוג
        /// </summary>
        public void InsertToLog()
        {
            DBService dbs = new DBService();
            //עדכון טבלת הלוג
            StrSql = $@"INSERT INTO {Lib1}.TAXILOGP (LTIMESTAMP,LDEPT,LPRESS,LACT,LEMP,LTAK,LWS,LPROD,LSTDTM)
                        values('{TimeStamp.ToString("yyyy-MM-dd-HH.mm.ss.00000")}',{Department.Trim()},'{MacName.Trim()}',   '{ActionCode}','{Oved}','{TakCode.ToString().PadLeft(2, '0')}','server','{Item.PadRight(15, ' ').Substring(0, 15)}','{Teken}') ";
            dbs.executeInsertQuery(StrSql);
        }

        /// <summary>
        /// הוספת רשומה בסוף משמרת ללוג - סגירת תקלות למשמרת
        /// </summary>
        public void InsertEndShiftToLog(DateTime now)
        {
            DBService dbs = new DBService();
            //עדכון טבלת הלוג
            StrSql = $@"INSERT INTO {Lib1}.TAXILOGP (LTIMESTAMP,LDEPT,LPRESS,LACT,LEMP,LTAK,LWS,LPROD,LSTDTM)
                        values('{now.ToString("yyyy-MM-dd-HH.mm.ss.00000")}',{Department.Trim()},'{MacName.Trim()}',   '4','{Oved}','{TakCode.ToString().PadLeft(2, '0')}','server','{Item.PadRight(15, ' ').Substring(0, 15)}','{Teken}') ";
            dbs.executeInsertQuery(StrSql);
        }

        public void UpdatePanelUnBlock(string remark)
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            StrSql = $@"INSERT INTO {Lib1}.TAXILOGP (LTIMESTAMP,LDEPT,LPRESS,LACT,LEMP,LTAK,LWS,LPROD,LSTDTM,LREMARK)
                        VALUES('{DateTime.Now.ToString("yyyy-MM-dd-HH.mm.ss.00000")}',{Department.Trim()},'{MacName.Trim()}',   '9','{Oved}','00','MANAGER','{Item.PadRight(15, ' ').Substring(0, 15)}','{Teken}','{remark}')";
            dbs.executeInsertQuery(StrSql);
        }

        public List<Press> GetPanelsToClose()
        {
            List<Press> P = new List<Press>();
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            string StrSql = $@"SELECT LPRESS,LTIMESTAMP
                               FROM {Lib1}.TAXILOGP
                               WHERE LTIMESTAMP BETWEEN '{DateTime.Now.AddHours(-1).ToString("yyyy-MM-dd-HH.mm.ss.00000")}' AND  '{DateTime.Now.AddMinutes(-30).ToString("yyyy-MM-dd-HH.mm.ss.00000")}'
                               AND LACT = 9 AND LTAK ='00'";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            foreach (DataRow row in DT.Rows)
            {
                Press prs = new Press(row["LPRESS"].ToString().Trim());
                prs.TimeStamp = DateTime.ParseExact(row["ltimestamp"].ToString().Trim().Substring(0, 19), "yyyy-MM-dd-HH.mm.ss", CultureInfo.InvariantCulture);
                P.Add(prs);
            }
            return P;
        }

        public void UpdatePanelClosed()
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            string StrSql = $@"UPDATE {Lib1}.TAXILOGP
                               SET LTAK='01' 
                               WHERE LPRESS = '{MacName}' AND LTIMESTAMP = '{TimeStamp.ToString("yyyy-MM-dd-HH.mm.ss.00000")}' AND LACT='9' AND LTAK='00'";
            dbs.executeUpdateQuery(StrSql);
        }

        /// <summary>
        /// הוספת גיפור לקובץ התבניות במערכת כדי לעקוב אחרי נקיון תבניות מלוכלכות
        /// </summary>
        public void AddCureToMold()
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            string StrSql = $@"update MFRT.TVNMHFL6
                                SET THMONE= THMONE + 1
                                WHERE THMHCH = '{MacName}'";
            dbs.executeUpdateQuery(StrSql);
        }

        /// <summary>
        /// Gets latest report to the press
        /// </summary>
        /// <returns></returns>
        public DataTable GetLastRow()
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            StrSql = $@"SELECT INSIZ,LEMP, lact,LTAK,ltimestamp,Lprod
                                    FROM {Lib1}.TAXILOGP
                                    Left join BPCSFALI.IIMN on left(LPROD,8)=INPROD 
                                    WHERE LPRESS='{MacName}'
                                    ORDER by ltimestamp desc fetch first row only";
            return DT = dbs.executeSelectQueryNoParam(StrSql);
        }

        /// <summary>
        /// בדיקה האם דיווח התקלה חולש על שתי משמרות
        /// </summary>
        /// <param name="timeStamp"></param>
        /// <returns></returns>
        public bool IsShiftOverLap(DateTime timeStamp)
        {
            string currnt = Form1.Shift(DateTime.Now.ToString("HHmm"));
            string prev = Form1.Shift(timeStamp.ToString("HHmm"));
            if (prev != currnt)
            {
                return true;
            }
            return false;
        }

        public DateTime GetShiftStart()
        {
            string shft = Form1.Shift(DateTime.Now.ToString("HHmm"));
            switch (shft)
            {
                case "1":
                    DateTime D = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    D = D.AddDays(-1);
                    TimeSpan T = new TimeSpan(23, 30, 00);
                    D = D.Date + T;
                    return D;

                case "2":
                    return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 06, 30, 00);
                case "3":
                    return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 15, 00, 00);
                default:return new DateTime();
            }
        }

        // תוספת גיפור על הבלדר הנוכחי בטבלאת הבלדרים
        public void AddCureToBladder()
        {
            DBService dbs = new DBService();
            StrSql = $@"UPDATE {tapiali}.BLDR
                        SET BDQTY = BDQTY + 1
                        WHERE BDMACH = '{MacName}'";
            dbs.executeUpdateQuery(StrSql);
        }

        public DataTable GetCatFromMCOVIP()
        {
            DataTable DT = new DataTable();
            DBService dbs = new DBService();
            StrSql = $@"SELECT distinct oprit as Item, trim(oprit)||' - '||insiz as Desc
                        FROM RZPALI.mcovip
                        LEFT JOIN BPCSFALI.iimn ON inprod=substring(oprit,1,8)
                        WHERE odate >= {DateTime.Now.ToString("1yyMMdd")} and omach = '{MacName}'";
            return DT = dbs.executeSelectQueryNoParam(StrSql);
        }
    }
}
