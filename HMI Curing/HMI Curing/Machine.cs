﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.ComponentModel;

namespace HMI_Curing
{


    public class Machine : INotifyPropertyChanged
    {
        // Fields
        string department, currentItem, macName;
        int workCenter;

        //Props
        public string MacName
        {
            get { return macName; }
            set { SetProperty(ref macName , value); }
        }

        public string CurrentItem
        {
            get { return currentItem; }
            set { currentItem = value; }
        }

        public string Department
        {
            get { return department; }
            set { department = value; }
        }

        public int WorkCenter
        {
            get { return workCenter; }
            set { workCenter = value; }
        }

        //Ctors
        public Machine()
        {

        }
        public Machine(string machName)
        {
            MacName = machName;
        }
        public Machine(string department, string currentItem, string macName, int workCenter)
        {
            this.Department = department;
            this.CurrentItem = currentItem;
            this.MacName = macName;
            this.WorkCenter = workCenter;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void SetProperty<T>(ref T field, T value, [CallerMemberName] string name = "")
        {
            if (!EqualityComparer<T>.Default.Equals(field, value))
            {
                field = value;
                var handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(name));
                }
            }
        }
    }

}
