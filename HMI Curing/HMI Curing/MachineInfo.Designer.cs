﻿using System;
using System.Threading;
using System.Threading.Tasks;
namespace HMI_Curing
{
    partial class MachineInfo
    {
        /// <summary> 
        /// RequiIndianRed designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// RequiIndianRed method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Lbl_Counter = new System.Windows.Forms.Label();
            this.SizeLBL = new System.Windows.Forms.Label();
            this.TimeLBL = new System.Windows.Forms.Label();
            this.NameLBL = new System.Windows.Forms.Label();
            this.Lbl_bottom = new System.Windows.Forms.Label();
            this.Lbl_Top = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Lbl_Counter
            // 
            this.Lbl_Counter.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_Counter.Location = new System.Drawing.Point(231, 24);
            this.Lbl_Counter.Name = "Lbl_Counter";
            this.Lbl_Counter.Size = new System.Drawing.Size(69, 22);
            this.Lbl_Counter.TabIndex = 12;
            this.Lbl_Counter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SizeLBL
            // 
            this.SizeLBL.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.SizeLBL.Location = new System.Drawing.Point(1, 46);
            this.SizeLBL.Name = "SizeLBL";
            this.SizeLBL.Size = new System.Drawing.Size(230, 20);
            this.SizeLBL.TabIndex = 11;
            this.SizeLBL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.SizeLBL.DoubleClick += new System.EventHandler(this.SizeLBL_DoubleClick);
            this.SizeLBL.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SizeLBL_MouseClick);
            // 
            // TimeLBL
            // 
            this.TimeLBL.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.TimeLBL.Location = new System.Drawing.Point(1, 25);
            this.TimeLBL.Name = "TimeLBL";
            this.TimeLBL.Size = new System.Drawing.Size(230, 20);
            this.TimeLBL.TabIndex = 10;
            this.TimeLBL.Text = "label2";
            this.TimeLBL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TimeLBL.DoubleClick += new System.EventHandler(this.TimeLBL_DoubleClick);
            this.TimeLBL.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TimeLBL_MouseClick);
            // 
            // NameLBL
            // 
            this.NameLBL.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.NameLBL.Location = new System.Drawing.Point(1, 3);
            this.NameLBL.Name = "NameLBL";
            this.NameLBL.Size = new System.Drawing.Size(230, 22);
            this.NameLBL.TabIndex = 9;
            this.NameLBL.Text = "label1";
            this.NameLBL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.NameLBL.DoubleClick += new System.EventHandler(this.NameLBL_DoubleClick);
            this.NameLBL.MouseClick += new System.Windows.Forms.MouseEventHandler(this.NameLBL_MouseClick);
            // 
            // Lbl_bottom
            // 
            this.Lbl_bottom.BackColor = System.Drawing.Color.Transparent;
            this.Lbl_bottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Lbl_bottom.Location = new System.Drawing.Point(0, 67);
            this.Lbl_bottom.Name = "Lbl_bottom";
            this.Lbl_bottom.Size = new System.Drawing.Size(302, 5);
            this.Lbl_bottom.TabIndex = 13;
            // 
            // Lbl_Top
            // 
            this.Lbl_Top.BackColor = System.Drawing.Color.Transparent;
            this.Lbl_Top.Dock = System.Windows.Forms.DockStyle.Top;
            this.Lbl_Top.Location = new System.Drawing.Point(0, 0);
            this.Lbl_Top.Name = "Lbl_Top";
            this.Lbl_Top.Size = new System.Drawing.Size(302, 3);
            this.Lbl_Top.TabIndex = 14;
            // 
            // MachineInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.Lbl_Top);
            this.Controls.Add(this.Lbl_bottom);
            this.Controls.Add(this.Lbl_Counter);
            this.Controls.Add(this.SizeLBL);
            this.Controls.Add(this.TimeLBL);
            this.Controls.Add(this.NameLBL);
            this.Margin = new System.Windows.Forms.Padding(4, 2, 2, 4);
            this.Name = "MachineInfo";
            this.Size = new System.Drawing.Size(302, 72);
            this.Load += new System.EventHandler(this.MachineInfo_Load);
            this.DoubleClick += new System.EventHandler(this.MachineInfo_DoubleClick);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label Lbl_Counter;
        private System.Windows.Forms.Label SizeLBL;
        private System.Windows.Forms.Label TimeLBL;
        private System.Windows.Forms.Label NameLBL;
        private System.Windows.Forms.Label Lbl_bottom;
        private System.Windows.Forms.Label Lbl_Top;
    }
}
