﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HMI_Curing
{
    public partial class InventoryInput : Form
    {
        
        public InventoryInput()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            
        }

        public string GetText()
        {
            return textBox1.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int n;
            bool isNumeric = int.TryParse(textBox1.Text, out n);
            if (isNumeric)
            {
                ///////// Login נחוץ כדי לשנות את סטטוס הפורם. רק ככה נוכל לתפוס את התוצאה ב 
                DialogResult = System.Windows.Forms.DialogResult.OK;
                Close();
            }
            else
            {
                MessageBox.Show("!אנא הזן מספר צמיגים אמיתי ברצפת הייצור");
            }
            
        }
    }
}
