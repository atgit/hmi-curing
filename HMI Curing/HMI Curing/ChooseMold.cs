﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using cwbx;

using ClassLibrary;

namespace HMI_Curing
{
    public partial class ChooseMold : Form
    {
        string mfrt = "MFRT";
        Press P;
        DataTable Items = new DataTable();
        List<string> result = new List<string>();
        string[] Mold;
        public ChooseMold()
        {
            InitializeComponent();
        }

        public ChooseMold(Press p)
        {
            P = p;
            InitializeComponent();
        }

        private void ChooseMold_Load(object sender, EventArgs e)
        {
            CB_Items.SelectedIndexChanged -= CB_Items_SelectedIndexChanged;
            FillCB_Items();
            CB_Items.SelectedIndexChanged += CB_Items_SelectedIndexChanged;
            Lbl_Title.Text = $@"בחירת תבנית למכבש {P.MacName}";
        }


        public List<string> GetItems()
        {
            cwbx.StringConverter stringConverter = new cwbx.StringConverter();
            result = new List<string>();

            // Define an AS400 system and connect to it
            AS400System system = new AS400System();
            system.Define("AS400");
            system.UserID = "as400";
            system.Password = "as400";
            system.IPAddress = "172.16.1.158";
            system.Connect(cwbcoServiceEnum.cwbcoServiceRemoteCmd);

            // Check the connection
            if (system.IsConnected(cwbcoServiceEnum.cwbcoServiceRemoteCmd) == 1)
            {
                // Create a program object and link to a system                
                cwbx.Program program = new cwbx.Program();
                program.LibraryName = "TAPIALI";
                program.ProgramName = "TXGPRC8";
                program.system = system;

                // Sample parameter data
                //string param = "Example";
                //int paramLength = 15; // Will be defined in the RGP program, so check with the programmer.
                string Dept = "0" + P.Department.PadRight(4, ' ');
                string Mach = P.MacName.PadRight(10, ' ');
                string Item = P.Item.PadRight(15, ' ');
                string Mold = "".PadRight(510, ' ');
                string ErrMsg = "".PadRight(30, ' ');
                // Create a collection of parameters associated with the program
                ProgramParameters parameters = new ProgramParameters();
                parameters.Append("Dept", cwbrcParameterTypeEnum.cwbrcInout, 4);
                parameters["Dept"].Value = stringConverter.ToBytes(Dept);
                parameters.Append("Mach", cwbrcParameterTypeEnum.cwbrcInout, 10);
                parameters["Mach"].Value = stringConverter.ToBytes(Mach);
                parameters.Append("Item", cwbrcParameterTypeEnum.cwbrcInout, 15);
                parameters["Item"].Value = stringConverter.ToBytes(Item);
                parameters.Append("Mold", cwbrcParameterTypeEnum.cwbrcInout, 510);
                parameters["Mold"].Value = stringConverter.ToBytes(Mold);
                parameters.Append("ErrMsg", cwbrcParameterTypeEnum.cwbrcInout, 30);
                parameters["ErrMsg"].Value = stringConverter.ToBytes(ErrMsg);

                // Finally call the program 
                try
                {
                    program.Call(parameters);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                foreach (cwbx.ProgramParameter item in parameters)
                {
                    result.Add(stringConverter.FromBytes(item.Value));
                }
            }
            system.Disconnect(cwbcoServiceEnum.cwbcoServiceAll);

            return result;
        }

        public void FillData()
        {
            Items.Reset();
            Items.Columns.Add("Item");
            Items.Columns.Add("Mold");
            Items.Columns.Add("Error");

            var Cat = result[2].Split(',').ToArray();
            Mold = result[3].Split(',').ToArray(); // משתנה גלובאלי כדי להשתמש בתבנית המלאה ולא בקיצור שמוצג
            var ErrM = result[4].Split(',').ToArray();

            for (int i = 0; i < Cat.Length; i++)
            {
                DataRow r = Items.NewRow();
                r["Item"] = CB_Items.SelectedValue;
                // בדיקה האם קיימת תבנית
                r["Mold"] = Mold[i].Trim().Length > 0 ? Mold[i].Trim().Substring(0, 9) : Mold[i].Trim();
                r["Error"] = ErrM[i].Trim();
                Items.Rows.Add(r);
            }
        }

        public void FillCB_Items()
        {
            CB_Items.DataSource = P.GetCatFromMCOVIP();
            CB_Items.ValueMember = "Item";
            CB_Items.DisplayMember = "Desc";
            CB_Items.SelectedIndex = -1;
        }

        private void CB_Items_SelectedIndexChanged(object sender, EventArgs e)
        {
            P.Item = CB_Items.SelectedValue.ToString();
            GetItems();
            FillData();
            DataView r = new DataView(Items, $@"Item = '{CB_Items.SelectedValue}'", "Item", DataViewRowState.CurrentRows);
            DGV_Molds.DataSource = r;
        }

        private void DGV_Molds_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DGV_Molds.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DGV_Molds.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            DGV_Molds.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            foreach (DataGridViewColumn column in DGV_Molds.Columns)
            {
                column.DefaultCellStyle.Font = new Font("Tahoma", 17F, GraphicsUnit.Pixel);
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            DGV_Molds.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 15F, FontStyle.Bold);
            DGV_Molds.Columns["Item"].HeaderText = "פריט";
            DGV_Molds.Columns["Mold"].HeaderText = "תבנית";
            DGV_Molds.Columns["Error"].HeaderText = "(שגיאה (אם יש";
        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            // הזנת התבנית הנבחרת 
            if (DGV_Molds.SelectedRows.Count != 0)
            {
                if (DGV_Molds.SelectedRows.Count == 1)
                {
                    if (!string.IsNullOrEmpty(DGV_Molds.SelectedRows[0].Cells["Mold"].Value.ToString()))
                    {
                        DeleteOldMold();
                        UpdateNewMold();
                        MessageBox.Show("התבנית נבחרה בהצלחה");
                        DialogResult = System.Windows.Forms.DialogResult.OK;
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("לא ניתן לבחור תבנית זו! נא טפל בשגיאה המוצגת");
                    }
                }
                else
                {
                    MessageBox.Show("לא ניתן לבחור יותר משורה אחת");
                }
            }
            else
            {
                MessageBox.Show("לא נבחרה שורה");
            }
        }

        private void CB_Items_Click(object sender, EventArgs e)
        {
            DataTable D = (DataTable)CB_Items.DataSource;
            if (D.Rows.Count == 0)
            {
                Items.Rows[0]["Desc"] = result[5];
                DGV_Molds.Refresh();
            }
        }

        public void UpdateNewMold()
        {
            string[] M = Mold[0].Split('-').ToArray();
            DataTable DT = new DataTable();
            DBService dbs = new DBService();
            string StrSql = $@"UPDATE {mfrt}.tvnmhfp
                               SET THMHCH = '{P.MacName}'
                               WHERE THTVDG = '{M[0]}' AND THTVNT = '{M[1]}' AND THTVNO = '{M[2]}' AND THDGM = '{M[3]}' AND
                               THSZCD = '{M[4]}' ";
            dbs.executeUpdateQuery(StrSql);
        }

        public void DeleteOldMold()
        {
            string[] M = Mold[0].Split('-').ToArray();
            DataTable DT = new DataTable();
            DBService dbs = new DBService();
            string StrSql = $@"UPDATE {mfrt}.tvnmhfl6
                               SET THMHCH = ''
                               WHERE THMHCH = '{P.MacName}' ";
            dbs.executeUpdateQuery(StrSql);
        }
    }
}
