﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HMI_Curing
{

    public partial class GipurEfficiency : Form
    {
        string SqlStr = "";
        Label[] LHeader;
        Panel PanelMain;
        PressInfo[,] LabelPressShift;

        public GipurEfficiency()
        {
            this.AutoScaleMode = AutoScaleMode.Font;
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_Test2_Load(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;
            DrawScreen();
            Getpresses();
            GetPressPerformance();
            Last3Shifts();
            ResizePressInfo();

            PanelMain.Focus();
            Cursor.Current = Cursors.PanEast;
        }

        private void OnMouseHover(object sender, EventArgs e)
        {
            PanelMain.Focus();
        }

        private void DrawScreen()
        {
            PanelMain = new Panel();
            PanelMain = new Panel();
            PanelMain.AutoScroll = true;
            PanelMain.Location = new Point(10, 95);
            PanelMain.Size = new Size(1600, 900);//new Size(1600, 900);
            PanelMain.MouseHover += new EventHandler(OnMouseHover);
            this.Controls.Add(PanelMain);

            LHeader = new Label[8];
            LabelPressShift = new PressInfo[8, 60];

            int LocX = 10;
            int TopLoc = 1;
            int RightLoc = 0;

            for (int J = 0; J < 8; J++)
            {
                LHeader[J] = new Label();
                LHeader[J].Location = new Point(LocX, 70);
                LHeader[J].BackColor = Color.WhiteSmoke;
                LHeader[J].TextAlign = ContentAlignment.MiddleCenter;
                LHeader[J].Font = new Font("Arial", 12, FontStyle.Bold);
                LHeader[J].BorderStyle = BorderStyle.Fixed3D;
                if (J == 0)
                {
                    LHeader[J].Size = new Size(100, 22);
                    LHeader[J].Text = "Press";
                    LocX += 105;
                }
                else
                {
                    LHeader[J].Size = new Size(200, 22);
                    if (J == 7)
                    {
                        LHeader[J].Text = "Last 3 Shifts";
                        LHeader[J].Location = new Point(LocX + 35, 70);
                    }
                    else
                        LHeader[J].Text = "Shift " + J.ToString();
                    LocX += 205;
                }
                this.Controls.Add(LHeader[J]);

                TopLoc = 2;

                for (int I = 0; I < 60; I++)
                {
                    LabelPressShift[J, I] = new PressInfo();
                    LabelPressShift[J, I].Location = new Point(RightLoc, TopLoc);
                    LabelPressShift[J, I].BackColor_Default();
                    LabelPressShift[J, I].TexlAlignment_Center();
                    if (J == 0)
                    {
                        LabelPressShift[J, I].Size = new Size(100, 24);
                        LabelPressShift[J, I].BorderStyle = BorderStyle.Fixed3D;
                        LabelPressShift[J, I].TexlAlignment_Left();
                    }
                    else
                    {
                        LabelPressShift[J, I].Size = new Size(200, 24);
                        if (J == 7)
                        {
                            LabelPressShift[J, I].Resize_Lbl_Press(200, 24);
                            LabelPressShift[J, I].Location = new Point(RightLoc, TopLoc);
                            LabelPressShift[J, I].TexlAlignment_Left();
                        }
                    }
                    PanelMain.Controls.Add(LabelPressShift[J, I]);
                    TopLoc += 29;
                }

                if (J == 0) RightLoc += 105;
                else if (J == 6) RightLoc += 240;
                else RightLoc += 205;
            }

            Label Lbl_Line = new Label();
            Lbl_Line.Location = new Point(RightLoc - 230, 2);
            Lbl_Line.Size = new Size(8, TopLoc);
            Lbl_Line.BackColor = Color.Black;
            PanelMain.Controls.Add(Lbl_Line);
        }

        private void ResizePressInfo()
        {
            int screenWidth = Screen.PrimaryScreen.Bounds.Width;
            int screenHeight = Screen.PrimaryScreen.Bounds.Height;
            int currentDPI = 0;
            using (Graphics g = this.CreateGraphics())
            {
                currentDPI = (int)g.DpiX;
            }
            if (currentDPI > 96)
            {
                foreach (var LBL in LabelPressShift)
                {
                    LBL.ResizeLBL(1);
                }
            }
            if (screenWidth < 1920 && screenWidth > 1600)
            {
                foreach (var LBL in LabelPressShift)
                {
                    LBL.ResizeLBL(1);
                }
            }
            else if (screenWidth <= 1600 && screenWidth > 1100)
            {
                foreach (var LBL in LabelPressShift)
                {
                    LBL.ResizeLBL(2);
                }
            }
        }

        private void Getpresses()
        {
            DBService dbs = new DBService();
            DataTable Result = new DataTable();
            SqlStr = "select SPRESS from RZPALI.TAXISTTP order by SSEQ";
            Result = dbs.executeSelectQueryNoParam(SqlStr);
            for (int I = 0; I < Result.Rows.Count; I++)
                LabelPressShift[0, I].SetPressNo(Result.Rows[I][0].ToString());
        }

        private void GetPressPerformance()
        {
            //int CShift = 0;
            if (DateTime.Now.TimeOfDay >= Convert.ToDateTime("06:30:00").TimeOfDay && DateTime.Now.TimeOfDay <= Convert.ToDateTime("14:59:59").TimeOfDay) //Shift = 2
            {   //CShift = 2;                                                
                FillTable(DateTime.Today.AddDays(-2).ToString("yyyyMMdd"), 2, 1);
                FillTable(DateTime.Today.AddDays(-2).ToString("yyyyMMdd"), 3, 2);
                FillTable(DateTime.Today.AddDays(-1).ToString("yyyyMMdd"), 1, 3);
                FillTable(DateTime.Today.AddDays(-1).ToString("yyyyMMdd"), 2, 4);
                FillTable(DateTime.Today.AddDays(-1).ToString("yyyyMMdd"), 3, 5);
                FillTable(DateTime.Today.ToString("yyyyMMdd"), 1, 6);
            }
            else if (DateTime.Now.TimeOfDay >= Convert.ToDateTime("15:00:00").TimeOfDay && DateTime.Now.TimeOfDay <= Convert.ToDateTime("23:29:59").TimeOfDay) //Shift = 3
            {   //CShift = 3;
                FillTable(DateTime.Today.AddDays(-2).ToString("yyyyMMdd"), 3, 1);
                FillTable(DateTime.Today.AddDays(-1).ToString("yyyyMMdd"), 1, 2);
                FillTable(DateTime.Today.AddDays(-1).ToString("yyyyMMdd"), 2, 3);
                FillTable(DateTime.Today.AddDays(-1).ToString("yyyyMMdd"), 3, 4);
                FillTable(DateTime.Today.ToString("yyyyMMdd"), 1, 5);
                FillTable(DateTime.Today.ToString("yyyyMMdd"), 2, 6);
            }
            else  //Shift = 1
            {   //CShift = 1;
                FillTable(DateTime.Today.AddDays(-2).ToString("yyyyMMdd"), 1, 1);
                FillTable(DateTime.Today.AddDays(-2).ToString("yyyyMMdd"), 2, 2);
                FillTable(DateTime.Today.AddDays(-2).ToString("yyyyMMdd"), 3, 3);
                FillTable(DateTime.Today.AddDays(-1).ToString("yyyyMMdd"), 1, 4);
                FillTable(DateTime.Today.AddDays(-1).ToString("yyyyMMdd"), 2, 5);
                FillTable(DateTime.Today.AddDays(-1).ToString("yyyyMMdd"), 3, 6);
            }
        }

        private void FillTable(string S_DT, int S_Shift, int Col_No)
        {
            decimal OPLAN = 0;
            decimal OMADE = 0;
            DBService dbs = new DBService();
            DataTable Result = new DataTable();

            LHeader[Col_No].Text = S_Shift + " - " + S_DT.Substring(6, 2) + "/" + S_DT.Substring(4, 2);


            SqlStr = @"select value(sum(QMULT * QPLAN), '0') as OPLAN,value(sum(TPCS), '0') as OMADE,SPRESS,SSEQ " +
                      "from RZPALI.TAXISTTP " +
                           "left join bpcsfv30.fltl01 on SPRESS=TMACH and(substring(TCLAS,1,1) between '1' and '9') and substring(TCLAS,2,1) in ('D','R')  and " +
                                                        "TDEPT IN(161, 162) and TTDTE=("+S_DT+" - 19280000) and TSHFT = '" + S_Shift + "' " +
                           "left join bpcsfali.shmaqv on QDATE="+S_DT+" and QSHFT='" + S_Shift + "' and SPRESS=QMACH " +
                      "group by SPRESS,SSEQ " +
                      "order by SSEQ";            
            Result = dbs.executeSelectQueryNoParam(SqlStr);            
            for (int I = 0; I < Result.Rows.Count; I++)
            {                
                OPLAN = Result.Rows[I]["OPLAN"].ToString() != "" ? Math.Floor(Convert.ToDecimal(Result.Rows[I]["OPLAN"])) : Convert.ToDecimal(0);
                OMADE = Result.Rows[I]["OMADE"].ToString() != "" ? Math.Round(Convert.ToDecimal(Result.Rows[I]["OMADE"])) : Convert.ToDecimal(0);
                LabelPressShift[Col_No, I].SetPressPerformance(OPLAN, OMADE);
                LabelPressShift[Col_No, I].ActualPlan = Result.Rows[0]["OPLAN"].ToString() != "" ? Convert.ToDecimal(Result.Rows[0]["OPLAN"]) : Convert.ToDecimal(0); // שמירת הנתון האמיתי - 
            }

            CkeckStopLack(S_DT, S_Shift, Col_No);


            //for (int I = 0; I < 60; I++)
            //{
            //    //SqlStr = @"select sum( QMULT*QPLAN) as OPLAN,sum(TPCS) as OMADE
            //    //            from bpcsfv30.fltl01 
            //    //            left join bpcsfali.shmaqv on QDATE=(TTDTE + 19280000) and QSHFT=TSHFT and TMACH=QMACH
            //    //            where TDEPT IN (161,162) and TTDTE=(" + S_DT + " - 19280000) and TSHFT='" + S_Shift + "' and TMACH='" + LabelPressShift[0, I].GetPressNo() + "' and (substring(TCLAS,1,1) between '1' and '9') and substring(TCLAS,2,1) in ('D','R') " +
            //    //            "group by TMACH,TTDTE,TSHFT ";

            //    //SqlStr = @"select sum( QMULT*QPLAN) as OPLAN,OMADE as OMADE
            //    //            from RZPALI.MCOVIP 
            //    //            left join bpcsfali.shmaqv on QDATE='20'||substring(ODATE,2,7) and QSHFT=OSHIFT and OMACH=QMACH
            //    //            where ODEPW IN (161,162) and ODATE=" + S_DT + " and OSHIFT='" + S_Shift + "' and OMACH='" + LabelPressShift[0, I].GetPressNo() + "'" +
            //    //       "GROUP BY OMADE ";

            //    Result = dbs.executeSelectQueryNoParam(SqlStr);
            //    if (Result.Rows.Count > 0)
            //    {
            //        OPLAN = Result.Rows[0]["OPLAN"].ToString() != "" ? Math.Floor(Convert.ToDecimal(Result.Rows[0]["OPLAN"])) : Convert.ToDecimal(0);
            //        OMADE = Result.Rows[0]["OMADE"].ToString() != "" ? Math.Round(Convert.ToDecimal(Result.Rows[0]["OMADE"])) : Convert.ToDecimal(0);
            //        LabelPressShift[Col_No, I].SetPressPerformance(OPLAN, OMADE);
            //        LabelPressShift[Col_No, I].ActualPlan = Result.Rows[0]["OPLAN"].ToString() != "" ? Convert.ToDecimal(Result.Rows[0]["OPLAN"]) : Convert.ToDecimal(0); // שמירת הנתון האמיתי - לא לעגל - כדי להשתמש אחר כך בסיכום 3 משמרות
            //        //CkeckStopLack(LabelPressShift[0, I].GetPressNo(), S_DT, S_Shift, Col_No, I);
            //    }
            //}
        }

        private void Last3Shifts()
        {
            decimal OPLAN = 0;
            decimal OMADE = 0;

            for (int I = 0; I < 60; I++)
            {
                OPLAN = Math.Floor(LabelPressShift[4, I].ActualPlan + LabelPressShift[5, I].ActualPlan + LabelPressShift[6, I].ActualPlan);
                OMADE = Convert.ToDecimal(LabelPressShift[4, I].GetPressNo().Substring(4, 4).Trim()) + Convert.ToDecimal(LabelPressShift[5, I].GetPressNo().Substring(4, 4).Trim()) + Convert.ToDecimal(LabelPressShift[6, I].GetPressNo().Substring(4, 4).Trim());

                LabelPressShift[7, I].SetPressPerformance(OPLAN, OMADE);
                CheckStopLast3(I);
            }
        }

        private void CkeckStopLack(string S_DT, int S_Shift, int Col_No)        
        {
            //bool Flg = false;
            int Flg = 0;
            DateTime DT = Convert.ToDateTime(S_DT.Substring(6, 2) + "/" + S_DT.Substring(4, 2) + "/20" + S_DT.Substring(2, 2));
            string DT_S = "";
            string DT_E = "";

            if (S_Shift == 1)
            {
                DT_S = DT.AddDays(-1).ToString("yyyy-MM-dd-23.30.00.000000");
                DT_E = DT.ToString("yyyy-MM-dd-06.29.59.000000");
            }
            else if (S_Shift == 2)
            {
                DT_S = DT.ToString("yyyy-MM-dd-06.30.00.000000");
                DT_E = DT.ToString("yyyy-MM-dd-14.59.59.000000");
            }
            else
            {
                DT_S = DT.ToString("yyyy-MM-dd-15.00.00.000000");
                DT_E = DT.ToString("yyyy-MM-dd-23.29.59.000000");
            }

            DBService dbs = new DBService();
            DataTable Result = new DataTable();

            SqlStr = "select SSEQ,SPRESS," +
                            "min(CASE when LTAK='01' then '1' END) as G1," +                            // בדיקה אם היה דיווח על חוסר צמיגים 
                            "min(CASE when LTAK='16' or LTAK ='18'or LTAK='19' then '1' END) as G2," +  // בדיקה אם היה דיווח על תקלה חשמלית מכנית או אינסטלציה
                            "min(CASE when LTAK='08' then '1' END) as G3," +                            // בדיקה אם היה דיווח על טיפול בפגם הנד"ת
                            "min(CASE when LTAK='04' or LTAK='07' then '1' END) as G4," +               // בדיקה אם היה דיווח על החלפת תבנית או החלפת בלדר
                            "min(CASE when LTAK='05' then '1'  END) as G5 " +                           // בדיקה אם היה דיווח על טיפול תבנית
                     "from RZPALI.TAXISTTP left join RZPALI.TAXILOGP on SPRESS = LPRESS and LTIMESTAMP between '" + DT_S + "' and '" + DT_E + "' and LACT = '4' " +
                     "group by SSEQ,SPRESS order by SSEQ";
            Result = dbs.executeSelectQueryNoParam(SqlStr);
            //Col_No = 0;
            for (int I = 0; I < Result.Rows.Count; I++)
            {
                Flg = 0;
                if (Result.Rows[I]["G1"].ToString() == "1")
                { Flg++; LabelPressShift[Col_No, I].StopLack_BackColor(Flg, Color.Cyan); }

                if (Result.Rows[I]["G2"].ToString() == "1")
                { Flg++; LabelPressShift[Col_No, I].StopLack_BackColor(Flg, Color.DodgerBlue); }

                if (Result.Rows[I]["G3"].ToString() == "1")
                { Flg++; LabelPressShift[Col_No, I].StopLack_BackColor(Flg, Color.FromArgb(255, 128, 255)); }

                if (Result.Rows[I]["G4"].ToString() == "1")
                { Flg++; LabelPressShift[Col_No, I].StopLack_BackColor(Flg, Color.DarkGoldenrod); }

                if (Result.Rows[I]["G5"].ToString() == "1")
                { Flg++; LabelPressShift[Col_No, I].StopLack_BackColor(Flg, Color.DarkGray); }
            }


            //// בדיקה אם היה דיווח על חוסר צמיגים בלבד
            //SqlStr = "select count(*) from RZPALI.TAXILOGP where LTIMESTAMP between '" + DT_S + "' and '" + DT_E + "' and LPRESS='" + PressNo + "' and LACT='4' and LTAK='01'";
            //Result = dbs.executeSelectQueryNoParam(SqlStr);
            //if (Convert.ToDecimal(Result.Rows[0][0]) > 0)
            //{
            //    Flg += 1;
            //    LabelPressShift[Col_No, Index].StopLack_BackColor(Flg, Color.Cyan);
            //}

            //// בדיקה אם היה דיווח על תקלה חשמלית מכנית או אינסטלציה
            //SqlStr = "select count(*) from RZPALI.TAXILOGP where LTIMESTAMP between '" + DT_S + "' and '" + DT_E + "' and LPRESS='" + PressNo + "' and LACT='4' and LTAK in ('16','18','19')";
            //Result = dbs.executeSelectQueryNoParam(SqlStr);
            //if (Convert.ToDecimal(Result.Rows[0][0]) > 0)
            //{
            //    Flg += 1;
            //    LabelPressShift[Col_No, Index].StopLack_BackColor(Flg, Color.DodgerBlue);
            //}

            //// בדיקה אם היה דיווח על טיפול בפגם הנד"ת
            //SqlStr = "select count(*) from RZPALI.TAXILOGP where LTIMESTAMP between '" + DT_S + "' and '" + DT_E + "' and LPRESS='" + PressNo + "' and LACT='4' and LTAK='08'";
            //Result = dbs.executeSelectQueryNoParam(SqlStr);
            //if (Convert.ToDecimal(Result.Rows[0][0]) > 0)
            //{
            //    Flg += 1;
            //    LabelPressShift[Col_No, Index].StopLack_BackColor(Flg, Color.FromArgb(255, 128, 255));
            //}

            //// בדיקה אם היה דיווח על החלפת תבנית או החלפת בלדר
            //SqlStr = "select count(*) from RZPALI.TAXILOGP where LTIMESTAMP between '" + DT_S + "' and '" + DT_E + "' and LPRESS='" + PressNo + "' and LACT='4' and LTAK in ('04','07')";
            //Result = dbs.executeSelectQueryNoParam(SqlStr);
            //if (Convert.ToDecimal(Result.Rows[0][0]) > 0)
            //{
            //    Flg += 1;
            //    LabelPressShift[Col_No, Index].StopLack_BackColor(Flg, Color.DarkGoldenrod);
            //}

            //// בדיקה אם היה דיווח על טיפול תבנית
            //SqlStr = "select count(*) from RZPALI.TAXILOGP where LTIMESTAMP between '" + DT_S + "' and '" + DT_E + "' and LPRESS='" + PressNo + "' and LACT='4' and LTAK='05'";
            //Result = dbs.executeSelectQueryNoParam(SqlStr);
            //if (Convert.ToDecimal(Result.Rows[0][0]) > 0)
            //{
            //    Flg += 1;
            //    LabelPressShift[Col_No, Index].StopLack_BackColor(Flg, Color.DarkGray);
            //}

        }

        // צביעת הלייבלים בעמודה האחרונה - 3 משמרות אחרונות 
        private void CheckStopLast3(int i)
        {
            int Flg = 0;
            foreach (Label item in LabelPressShift[6, i].Controls)
            {
                if (item.BackColor != Color.IndianRed && item.BackColor != Color.Green && item.BackColor != Color.Yellow && item.BackColor != Color.LightGray)
                {
                    Flg++;
                    LabelPressShift[7, i].StopLackLast3_BackColor(Flg, item.BackColor);
                }
            }

            foreach (Label item in LabelPressShift[5, i].Controls)
            {
                if (item.BackColor != Color.IndianRed && item.BackColor != Color.Green && item.BackColor != Color.Yellow && item.BackColor != Color.LightGray)
                {
                    Flg++;
                    LabelPressShift[7, i].StopLackLast3_BackColor(Flg, item.BackColor);
                }
            }

            foreach (Label item in LabelPressShift[4, i].Controls)
            {
                if (item.BackColor != Color.IndianRed && item.BackColor != Color.Green && item.BackColor != Color.Yellow && item.BackColor != Color.LightGray)
                {
                    Flg++;
                    LabelPressShift[7, i].StopLackLast3_BackColor(Flg, item.BackColor);
                }
            }

        }

        private void label4_MouseHover(object sender, EventArgs e)
        {
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(this.label4, "No Green Tire");
        }

        private void label8_MouseHover(object sender, EventArgs e)
        {
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(this.label8, "Break Down: Electricity, Mechanical, Plumbing");
        }

        private void label10_MouseHover(object sender, EventArgs e)
        {
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(this.label10, "Quality: Process Engineering");
        }

        private void label12_MouseHover(object sender, EventArgs e)
        {
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(this.label12, "Mold / Bladder Change");
        }

        private void label14_MouseHover(object sender, EventArgs e)
        {
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(this.label14, "Mold Maintenance");
        }

    }
}
