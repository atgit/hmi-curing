﻿namespace HMI_Curing
{
    partial class UnlockPress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_OK = new System.Windows.Forms.Button();
            this.Lbl_Title = new System.Windows.Forms.Label();
            this.Txt_Verification = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Btn_OK
            // 
            this.Btn_OK.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Btn_OK.Location = new System.Drawing.Point(98, 131);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(75, 23);
            this.Btn_OK.TabIndex = 2;
            this.Btn_OK.Text = "אישור";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // Lbl_Title
            // 
            this.Lbl_Title.AutoSize = true;
            this.Lbl_Title.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_Title.Location = new System.Drawing.Point(12, 23);
            this.Lbl_Title.MaximumSize = new System.Drawing.Size(300, 0);
            this.Lbl_Title.Name = "Lbl_Title";
            this.Lbl_Title.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Lbl_Title.Size = new System.Drawing.Size(251, 32);
            this.Lbl_Title.TabIndex = 6;
            this.Lbl_Title.Text = "אנא הזן את קוד האישור מהודעת ה SMS שנשלחה למכשירך:";
            // 
            // Txt_Verification
            // 
            this.Txt_Verification.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Txt_Verification.Location = new System.Drawing.Point(36, 74);
            this.Txt_Verification.MaxLength = 6;
            this.Txt_Verification.Name = "Txt_Verification";
            this.Txt_Verification.Size = new System.Drawing.Size(208, 33);
            this.Txt_Verification.TabIndex = 1;
            this.Txt_Verification.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_Verification.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Txt_Verification_KeyDown);
            this.Txt_Verification.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Verification_KeyPress);
            // 
            // UnlockPress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.ClientSize = new System.Drawing.Size(284, 166);
            this.Controls.Add(this.Txt_Verification);
            this.Controls.Add(this.Lbl_Title);
            this.Controls.Add(this.Btn_OK);
            this.Name = "UnlockPress";
            this.Text = "UnlockPress";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.Label Lbl_Title;
        private System.Windows.Forms.TextBox Txt_Verification;
    }
}